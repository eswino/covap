EESchema Schematic File Version 4
EELAYER 30 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 2 9
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L power:GND #PWR?
U 1 1 600AF055
P 2600 4350
AR Path="/600AF055" Ref="#PWR?"  Part="1" 
AR Path="/5F2CBBC0/600AF055" Ref="#PWR?"  Part="1" 
AR Path="/5F9CF0DB/600AF055" Ref="#PWR?"  Part="1" 
AR Path="/60088CAB/600AF055" Ref="#PWR04"  Part="1" 
AR Path="/60C45676/600AF055" Ref="#PWR02"  Part="1" 
F 0 "#PWR02" H 2600 4100 50  0001 C CNN
F 1 "GND" H 2605 4177 50  0000 C CNN
F 2 "" H 2600 4350 50  0001 C CNN
F 3 "" H 2600 4350 50  0001 C CNN
	1    2600 4350
	1    0    0    -1  
$EndComp
$Comp
L Device:C_Small C?
U 1 1 600AF05B
P 5300 7050
AR Path="/600AF05B" Ref="C?"  Part="1" 
AR Path="/5F2CBBC0/600AF05B" Ref="C?"  Part="1" 
AR Path="/5F9CF0DB/600AF05B" Ref="C?"  Part="1" 
AR Path="/60088CAB/600AF05B" Ref="C3"  Part="1" 
AR Path="/60C45676/600AF05B" Ref="C1"  Part="1" 
F 0 "C1" H 5392 7096 50  0000 L CNN
F 1 "NC" H 5392 7005 50  0000 L CNN
F 2 "Capacitor_SMD:C_0603_1608Metric_Pad1.05x0.95mm_HandSolder" H 5300 7050 50  0001 C CNN
F 3 "~" H 5300 7050 50  0001 C CNN
	1    5300 7050
	1    0    0    -1  
$EndComp
$Comp
L Device:C_Small C?
U 1 1 600AF061
P 5600 7050
AR Path="/600AF061" Ref="C?"  Part="1" 
AR Path="/5F2CBBC0/600AF061" Ref="C?"  Part="1" 
AR Path="/5F9CF0DB/600AF061" Ref="C?"  Part="1" 
AR Path="/60088CAB/600AF061" Ref="C5"  Part="1" 
AR Path="/60C45676/600AF061" Ref="C2"  Part="1" 
F 0 "C2" H 5692 7096 50  0000 L CNN
F 1 "NC" H 5692 7005 50  0000 L CNN
F 2 "Capacitor_SMD:C_0603_1608Metric_Pad1.05x0.95mm_HandSolder" H 5600 7050 50  0001 C CNN
F 3 "~" H 5600 7050 50  0001 C CNN
	1    5600 7050
	1    0    0    -1  
$EndComp
$Comp
L Device:R_Small R?
U 1 1 600AF067
P 5450 6900
AR Path="/600AF067" Ref="R?"  Part="1" 
AR Path="/5F2CBBC0/600AF067" Ref="R?"  Part="1" 
AR Path="/5F9CF0DB/600AF067" Ref="R?"  Part="1" 
AR Path="/60088CAB/600AF067" Ref="R6"  Part="1" 
AR Path="/60C45676/600AF067" Ref="R4"  Part="1" 
F 0 "R4" V 5400 6900 50  0000 C CNN
F 1 "0" V 5450 6900 50  0000 C CNN
F 2 "Resistor_SMD:R_0603_1608Metric_Pad1.05x0.95mm_HandSolder" H 5450 6900 50  0001 C CNN
F 3 "~" H 5450 6900 50  0001 C CNN
	1    5450 6900
	0    1    1    0   
$EndComp
Wire Wire Line
	5300 6950 5300 6900
Wire Wire Line
	5300 6900 5350 6900
Wire Wire Line
	5550 6900 5600 6900
Wire Wire Line
	5600 6900 5600 6950
Wire Wire Line
	5300 7150 5300 7200
Wire Wire Line
	5300 7200 5600 7200
Wire Wire Line
	5600 7200 5600 7150
$Comp
L power:GND #PWR?
U 1 1 600AF074
P 5600 7250
AR Path="/600AF074" Ref="#PWR?"  Part="1" 
AR Path="/5F2CBBC0/600AF074" Ref="#PWR?"  Part="1" 
AR Path="/5F9CF0DB/600AF074" Ref="#PWR?"  Part="1" 
AR Path="/60088CAB/600AF074" Ref="#PWR06"  Part="1" 
AR Path="/60C45676/600AF074" Ref="#PWR010"  Part="1" 
F 0 "#PWR010" H 5600 7000 50  0001 C CNN
F 1 "GND" H 5605 7077 50  0000 C CNN
F 2 "" H 5600 7250 50  0001 C CNN
F 3 "" H 5600 7250 50  0001 C CNN
	1    5600 7250
	1    0    0    -1  
$EndComp
Connection ~ 5600 7200
$Comp
L Connector:Conn_Coaxial J?
U 1 1 600AF07B
P 6100 6900
AR Path="/600AF07B" Ref="J?"  Part="1" 
AR Path="/5F2CBBC0/600AF07B" Ref="J?"  Part="1" 
AR Path="/5F9CF0DB/600AF07B" Ref="J?"  Part="1" 
AR Path="/60088CAB/600AF07B" Ref="J1"  Part="1" 
AR Path="/60C45676/600AF07B" Ref="J2"  Part="1" 
F 0 "J2" H 6200 6876 50  0000 L CNN
F 1 "868 MHz" H 6200 6785 50  0000 L CNN
F 2 "Connector_Coaxial:U.FL_Molex_MCRF_73412-0110_Vertical" H 6100 6900 50  0001 C CNN
F 3 " ~" H 6100 6900 50  0001 C CNN
	1    6100 6900
	1    0    0    -1  
$EndComp
Wire Wire Line
	5600 6900 5900 6900
Connection ~ 5600 6900
Wire Wire Line
	5600 7200 6100 7200
Wire Wire Line
	6100 7200 6100 7100
$Comp
L power:GND #PWR?
U 1 1 600AF085
P 9750 1350
AR Path="/600AF085" Ref="#PWR?"  Part="1" 
AR Path="/5F2CBBC0/600AF085" Ref="#PWR?"  Part="1" 
AR Path="/5F9CF0DB/600AF085" Ref="#PWR?"  Part="1" 
AR Path="/60088CAB/600AF085" Ref="#PWR020"  Part="1" 
AR Path="/60C45676/600AF085" Ref="#PWR032"  Part="1" 
F 0 "#PWR032" H 9750 1100 50  0001 C CNN
F 1 "GND" H 9755 1177 50  0000 C CNN
F 2 "" H 9750 1350 50  0001 C CNN
F 3 "" H 9750 1350 50  0001 C CNN
	1    9750 1350
	1    0    0    -1  
$EndComp
$Comp
L power:+3.3V #PWR?
U 1 1 600AF08B
P 9850 850
AR Path="/600AF08B" Ref="#PWR?"  Part="1" 
AR Path="/5F2CBBC0/600AF08B" Ref="#PWR?"  Part="1" 
AR Path="/5F9CF0DB/600AF08B" Ref="#PWR?"  Part="1" 
AR Path="/60088CAB/600AF08B" Ref="#PWR021"  Part="1" 
AR Path="/60C45676/600AF08B" Ref="#PWR033"  Part="1" 
F 0 "#PWR033" H 9850 700 50  0001 C CNN
F 1 "+3.3V" H 9865 1023 50  0000 C CNN
F 2 "" H 9850 850 50  0001 C CNN
F 3 "" H 9850 850 50  0001 C CNN
	1    9850 850 
	1    0    0    -1  
$EndComp
NoConn ~ 10500 1100
NoConn ~ 10500 1200
Wire Wire Line
	9850 850  9850 900 
Wire Wire Line
	9850 900  10000 900 
Wire Wire Line
	9750 1000 10000 1000
Wire Wire Line
	10000 1300 9750 1300
Connection ~ 9750 1300
Wire Wire Line
	9750 1300 9750 1350
Wire Wire Line
	10000 1100 9750 1100
Wire Wire Line
	9750 1000 9750 1100
Connection ~ 9750 1100
Wire Wire Line
	9750 1100 9750 1300
$Comp
L Connector_Generic:Conn_02x05_Odd_Even J?
U 1 1 600AF09D
P 10200 1100
AR Path="/600AF09D" Ref="J?"  Part="1" 
AR Path="/60088CAB/600AF09D" Ref="J5"  Part="1" 
AR Path="/60C45676/600AF09D" Ref="J4"  Part="1" 
F 0 "J4" H 10250 1517 50  0000 C CNN
F 1 "JTAG_LRWAN" H 10250 1426 50  0000 C CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_2x05_P2.54mm_Vertical" H 10200 1100 50  0001 C CNN
F 3 "~" H 10200 1100 50  0001 C CNN
	1    10200 1100
	1    0    0    -1  
$EndComp
NoConn ~ 10000 1200
$Comp
L Device:C_Small C?
U 1 1 600AF0A4
P 9100 2700
AR Path="/600AF0A4" Ref="C?"  Part="1" 
AR Path="/5F2CBBC0/600AF0A4" Ref="C?"  Part="1" 
AR Path="/5F9CF0DB/600AF0A4" Ref="C?"  Part="1" 
AR Path="/60088CAB/600AF0A4" Ref="C10"  Part="1" 
AR Path="/60C45676/600AF0A4" Ref="C11"  Part="1" 
F 0 "C11" H 9150 2800 50  0000 L CNN
F 1 "10uF" H 9150 2600 50  0000 L CNN
F 2 "Capacitor_SMD:C_0805_2012Metric_Pad1.15x1.40mm_HandSolder" H 9100 2700 50  0001 C CNN
F 3 "~" H 9100 2700 50  0001 C CNN
	1    9100 2700
	1    0    0    -1  
$EndComp
$Comp
L Device:C_Small C?
U 1 1 600AF0AA
P 8350 2700
AR Path="/600AF0AA" Ref="C?"  Part="1" 
AR Path="/5F2CBBC0/600AF0AA" Ref="C?"  Part="1" 
AR Path="/5F9CF0DB/600AF0AA" Ref="C?"  Part="1" 
AR Path="/60088CAB/600AF0AA" Ref="C4"  Part="1" 
AR Path="/60C45676/600AF0AA" Ref="C5"  Part="1" 
F 0 "C5" H 8300 2950 50  0000 L CNN
F 1 "100nF" H 8250 2350 50  0000 L CNN
F 2 "Capacitor_SMD:C_0603_1608Metric_Pad1.05x0.95mm_HandSolder" H 8350 2700 50  0001 C CNN
F 3 "~" H 8350 2700 50  0001 C CNN
	1    8350 2700
	1    0    0    -1  
$EndComp
$Comp
L Device:C_Small C?
U 1 1 600AF0B0
P 8650 2700
AR Path="/600AF0B0" Ref="C?"  Part="1" 
AR Path="/5F2CBBC0/600AF0B0" Ref="C?"  Part="1" 
AR Path="/5F9CF0DB/600AF0B0" Ref="C?"  Part="1" 
AR Path="/60088CAB/600AF0B0" Ref="C7"  Part="1" 
AR Path="/60C45676/600AF0B0" Ref="C7"  Part="1" 
F 0 "C7" H 8600 2950 50  0000 L CNN
F 1 "100nF" H 8550 2350 50  0000 L CNN
F 2 "Capacitor_SMD:C_0603_1608Metric_Pad1.05x0.95mm_HandSolder" H 8650 2700 50  0001 C CNN
F 3 "~" H 8650 2700 50  0001 C CNN
	1    8650 2700
	1    0    0    -1  
$EndComp
$Comp
L Device:C_Small C?
U 1 1 600AF0B6
P 8500 2700
AR Path="/600AF0B6" Ref="C?"  Part="1" 
AR Path="/5F2CBBC0/600AF0B6" Ref="C?"  Part="1" 
AR Path="/5F9CF0DB/600AF0B6" Ref="C?"  Part="1" 
AR Path="/60088CAB/600AF0B6" Ref="C6"  Part="1" 
AR Path="/60C45676/600AF0B6" Ref="C6"  Part="1" 
F 0 "C6" H 8450 2950 50  0000 L CNN
F 1 "100nF" H 8400 2450 50  0000 L CNN
F 2 "Capacitor_SMD:C_0603_1608Metric_Pad1.05x0.95mm_HandSolder" H 8500 2700 50  0001 C CNN
F 3 "~" H 8500 2700 50  0001 C CNN
	1    8500 2700
	1    0    0    -1  
$EndComp
$Comp
L Device:C_Small C?
U 1 1 600AF0BC
P 8800 2700
AR Path="/600AF0BC" Ref="C?"  Part="1" 
AR Path="/5F2CBBC0/600AF0BC" Ref="C?"  Part="1" 
AR Path="/5F9CF0DB/600AF0BC" Ref="C?"  Part="1" 
AR Path="/60088CAB/600AF0BC" Ref="C8"  Part="1" 
AR Path="/60C45676/600AF0BC" Ref="C8"  Part="1" 
F 0 "C8" H 8750 2950 50  0000 L CNN
F 1 "1uF" H 8750 2450 50  0000 L CNN
F 2 "Capacitor_SMD:C_0805_2012Metric_Pad1.15x1.40mm_HandSolder" H 8800 2700 50  0001 C CNN
F 3 "~" H 8800 2700 50  0001 C CNN
	1    8800 2700
	1    0    0    -1  
$EndComp
$Comp
L Device:C_Small C?
U 1 1 600AF0C2
P 8950 2700
AR Path="/600AF0C2" Ref="C?"  Part="1" 
AR Path="/5F2CBBC0/600AF0C2" Ref="C?"  Part="1" 
AR Path="/5F9CF0DB/600AF0C2" Ref="C?"  Part="1" 
AR Path="/60088CAB/600AF0C2" Ref="C9"  Part="1" 
AR Path="/60C45676/600AF0C2" Ref="C9"  Part="1" 
F 0 "C9" H 8900 2950 50  0000 L CNN
F 1 "10uF" H 8850 2350 50  0000 L CNN
F 2 "Capacitor_SMD:C_0805_2012Metric_Pad1.15x1.40mm_HandSolder" H 8950 2700 50  0001 C CNN
F 3 "~" H 8950 2700 50  0001 C CNN
	1    8950 2700
	1    0    0    -1  
$EndComp
Wire Wire Line
	2600 4350 2600 4250
Wire Wire Line
	2400 1100 2400 1250
Wire Wire Line
	2500 1250 2500 1100
Wire Wire Line
	2400 1100 2500 1100
Wire Wire Line
	2600 1250 2600 1100
Wire Wire Line
	2800 1100 2800 1250
Wire Wire Line
	8350 2800 8350 2900
Wire Wire Line
	8350 2900 8500 2900
Wire Wire Line
	9100 2900 9100 2800
Wire Wire Line
	8950 2800 8950 2900
Connection ~ 8950 2900
Wire Wire Line
	8950 2900 9100 2900
Wire Wire Line
	8500 2800 8500 2900
Connection ~ 8500 2900
Wire Wire Line
	8500 2900 8650 2900
Wire Wire Line
	8650 2800 8650 2900
Connection ~ 8650 2900
Wire Wire Line
	8650 2900 8800 2900
Wire Wire Line
	8800 2800 8800 2900
Connection ~ 8800 2900
Wire Wire Line
	8800 2900 8950 2900
$Comp
L power:GND #PWR?
U 1 1 600AF0F6
P 9100 2950
AR Path="/600AF0F6" Ref="#PWR?"  Part="1" 
AR Path="/60088CAB/600AF0F6" Ref="#PWR07"  Part="1" 
AR Path="/60C45676/600AF0F6" Ref="#PWR031"  Part="1" 
F 0 "#PWR031" H 9100 2700 50  0001 C CNN
F 1 "GND" H 9105 2777 50  0000 C CNN
F 2 "" H 9100 2950 50  0001 C CNN
F 3 "" H 9100 2950 50  0001 C CNN
	1    9100 2950
	1    0    0    -1  
$EndComp
Wire Wire Line
	9100 2900 9100 2950
Connection ~ 9100 2900
$Comp
L power:+3.3V #PWR?
U 1 1 600AF0FE
P 2400 1100
AR Path="/600AF0FE" Ref="#PWR?"  Part="1" 
AR Path="/60088CAB/600AF0FE" Ref="#PWR03"  Part="1" 
AR Path="/60C45676/600AF0FE" Ref="#PWR01"  Part="1" 
F 0 "#PWR01" H 2400 950 50  0001 C CNN
F 1 "+3.3V" H 2415 1273 50  0000 C CNN
F 2 "" H 2400 1100 50  0001 C CNN
F 3 "" H 2400 1100 50  0001 C CNN
	1    2400 1100
	1    0    0    -1  
$EndComp
Connection ~ 2400 1100
Wire Wire Line
	5200 6900 5300 6900
Connection ~ 5300 6900
Wire Wire Line
	3500 2250 3750 2250
Text GLabel 3750 2250 2    50   Input ~ 0
PB8|SCL
Wire Wire Line
	3500 2350 3750 2350
Text GLabel 3750 2350 2    50   Input ~ 0
PB9|SDA
Wire Wire Line
	1700 1850 1450 1850
Text GLabel 1450 1850 0    50   Input ~ 0
PA0|A4
Wire Wire Line
	1700 2050 1450 2050
Text GLabel 1450 2050 0    50   Input ~ 0
PA3|A3
Wire Wire Line
	1700 1950 1450 1950
Text GLabel 1450 1950 0    50   Input ~ 0
PA2|A2
Wire Wire Line
	1700 2150 1450 2150
Text GLabel 1450 2150 0    50   Input ~ 0
PA4|A0
Wire Wire Line
	1700 2250 1450 2250
Text GLabel 1450 2250 0    50   Input ~ 0
PA5|A1
Wire Wire Line
	1700 2850 1450 2850
Text GLabel 1450 2850 0    50   Input ~ 0
PA13|SWDIO
Wire Wire Line
	1700 2950 1450 2950
Text GLabel 1450 2950 0    50   Input ~ 0
PA14|SWCLK
Text GLabel 1450 2450 0    50   Input ~ 0
PA9|D1
Text GLabel 1450 2550 0    50   Input ~ 0
PA10|D0
Wire Wire Line
	1450 2450 1700 2450
Wire Wire Line
	1450 2550 1700 2550
$Comp
L Switch:SW_Push swReset?
U 1 1 600AF137
P 5900 2900
AR Path="/600AF137" Ref="swReset?"  Part="1" 
AR Path="/60088CAB/600AF137" Ref="swReset1"  Part="1" 
AR Path="/60C45676/600AF137" Ref="swReset1"  Part="1" 
F 0 "swReset1" V 6150 3250 50  0000 R CNN
F 1 "RESET" V 6250 3250 50  0000 R CNN
F 2 "Button_Switch_SMD:SW_SPST_CK_RS282G05A3" H 5900 3100 50  0001 C CNN
F 3 "~" H 5900 3100 50  0001 C CNN
	1    5900 2900
	0    -1   -1   0   
$EndComp
$Comp
L Device:R_Small R?
U 1 1 600AF13D
P 6050 2500
AR Path="/600AF13D" Ref="R?"  Part="1" 
AR Path="/60088CAB/600AF13D" Ref="R3"  Part="1" 
AR Path="/60C45676/600AF13D" Ref="R6"  Part="1" 
F 0 "R6" H 6109 2546 50  0000 L CNN
F 1 "100k" H 6109 2455 50  0000 L CNN
F 2 "Resistor_SMD:R_0805_2012Metric_Pad1.15x1.40mm_HandSolder" H 6050 2500 50  0001 C CNN
F 3 "~" H 6050 2500 50  0001 C CNN
	1    6050 2500
	1    0    0    -1  
$EndComp
$Comp
L Device:C_Small C?
U 1 1 600AF143
P 6050 2850
AR Path="/600AF143" Ref="C?"  Part="1" 
AR Path="/60088CAB/600AF143" Ref="C12"  Part="1" 
AR Path="/60C45676/600AF143" Ref="C3"  Part="1" 
F 0 "C3" H 6142 2896 50  0000 L CNN
F 1 "100nF" H 6142 2805 50  0000 L CNN
F 2 "Capacitor_SMD:C_0603_1608Metric_Pad1.05x0.95mm_HandSolder" H 6050 2850 50  0001 C CNN
F 3 "~" H 6050 2850 50  0001 C CNN
	1    6050 2850
	1    0    0    -1  
$EndComp
Wire Wire Line
	5900 2700 5900 2650
Wire Wire Line
	5900 2650 6050 2650
Wire Wire Line
	6050 2650 6050 2600
Wire Wire Line
	6050 2750 6050 2650
Connection ~ 6050 2650
Wire Wire Line
	6050 2950 6050 3100
Wire Wire Line
	6050 3100 5900 3100
Wire Wire Line
	6050 3100 6050 3200
Connection ~ 6050 3100
$Comp
L power:GND #PWR?
U 1 1 600AF152
P 6050 3200
AR Path="/600AF152" Ref="#PWR?"  Part="1" 
AR Path="/5F2CBBC0/600AF152" Ref="#PWR?"  Part="1" 
AR Path="/5F9CF0DB/600AF152" Ref="#PWR?"  Part="1" 
AR Path="/60088CAB/600AF152" Ref="#PWR013"  Part="1" 
AR Path="/60C45676/600AF152" Ref="#PWR015"  Part="1" 
F 0 "#PWR015" H 6050 2950 50  0001 C CNN
F 1 "GND" H 6055 3027 50  0000 C CNN
F 2 "" H 6050 3200 50  0001 C CNN
F 3 "" H 6050 3200 50  0001 C CNN
	1    6050 3200
	1    0    0    -1  
$EndComp
$Comp
L power:+3.3V #PWR?
U 1 1 600AF158
P 6050 2350
AR Path="/600AF158" Ref="#PWR?"  Part="1" 
AR Path="/60088CAB/600AF158" Ref="#PWR012"  Part="1" 
AR Path="/60C45676/600AF158" Ref="#PWR014"  Part="1" 
F 0 "#PWR014" H 6050 2200 50  0001 C CNN
F 1 "+3.3V" H 6065 2523 50  0000 C CNN
F 2 "" H 6050 2350 50  0001 C CNN
F 3 "" H 6050 2350 50  0001 C CNN
	1    6050 2350
	1    0    0    -1  
$EndComp
Wire Wire Line
	6050 2350 6050 2400
$Comp
L Switch:SW_Push swBoot?
U 1 1 600AF15F
P 5750 850
AR Path="/600AF15F" Ref="swBoot?"  Part="1" 
AR Path="/60088CAB/600AF15F" Ref="swBoot1"  Part="1" 
AR Path="/60C45676/600AF15F" Ref="swBoot1"  Part="1" 
F 0 "swBoot1" H 5850 750 50  0000 R CNN
F 1 "BOOT0" H 5800 650 50  0000 R CNN
F 2 "Button_Switch_SMD:SW_SPST_CK_RS282G05A3" H 5750 1050 50  0001 C CNN
F 3 "~" H 5750 1050 50  0001 C CNN
	1    5750 850 
	1    0    0    -1  
$EndComp
$Comp
L Device:R_Small R?
U 1 1 600AF165
P 6000 1200
AR Path="/600AF165" Ref="R?"  Part="1" 
AR Path="/60088CAB/600AF165" Ref="R4"  Part="1" 
AR Path="/60C45676/600AF165" Ref="R5"  Part="1" 
F 0 "R5" H 6059 1246 50  0000 L CNN
F 1 "100k" H 6059 1155 50  0000 L CNN
F 2 "Resistor_SMD:R_0805_2012Metric_Pad1.15x1.40mm_HandSolder" H 6000 1200 50  0001 C CNN
F 3 "~" H 6000 1200 50  0001 C CNN
	1    6000 1200
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR?
U 1 1 600AF16E
P 6000 1500
AR Path="/600AF16E" Ref="#PWR?"  Part="1" 
AR Path="/5F2CBBC0/600AF16E" Ref="#PWR?"  Part="1" 
AR Path="/5F9CF0DB/600AF16E" Ref="#PWR?"  Part="1" 
AR Path="/60088CAB/600AF16E" Ref="#PWR019"  Part="1" 
AR Path="/60C45676/600AF16E" Ref="#PWR013"  Part="1" 
F 0 "#PWR013" H 6000 1250 50  0001 C CNN
F 1 "GND" H 6005 1327 50  0000 C CNN
F 2 "" H 6000 1500 50  0001 C CNN
F 3 "" H 6000 1500 50  0001 C CNN
	1    6000 1500
	1    0    0    -1  
$EndComp
Wire Wire Line
	6000 850  6000 1100
$Comp
L power:+3.3V #PWR?
U 1 1 600AF17B
P 5500 800
AR Path="/600AF17B" Ref="#PWR?"  Part="1" 
AR Path="/60088CAB/600AF17B" Ref="#PWR018"  Part="1" 
AR Path="/60C45676/600AF17B" Ref="#PWR09"  Part="1" 
F 0 "#PWR09" H 5500 650 50  0001 C CNN
F 1 "+3.3V" H 5515 973 50  0000 C CNN
F 2 "" H 5500 800 50  0001 C CNN
F 3 "" H 5500 800 50  0001 C CNN
	1    5500 800 
	1    0    0    -1  
$EndComp
Text GLabel 1450 3150 0    50   Input ~ 0
RESET
Text GLabel 1450 4050 0    50   Input ~ 0
BOOT0
Wire Wire Line
	1450 4050 1700 4050
Wire Wire Line
	1450 3150 1700 3150
Text GLabel 6350 2650 2    50   Input ~ 0
RESET
Wire Wire Line
	6350 2650 6050 2650
Text GLabel 6250 850  2    50   Input ~ 0
BOOT0
Wire Wire Line
	6250 850  6000 850 
Text GLabel 1450 2650 0    50   Input ~ 0
USB-
Text GLabel 1450 2750 0    50   Input ~ 0
USB+
Wire Wire Line
	1450 2650 1700 2650
Wire Wire Line
	1450 2750 1700 2750
$Comp
L power:GND #PWR?
U 1 1 600AF199
P 5950 5500
AR Path="/600AF199" Ref="#PWR?"  Part="1" 
AR Path="/5F2CBBC0/600AF199" Ref="#PWR?"  Part="1" 
AR Path="/5F9CF0DB/600AF199" Ref="#PWR?"  Part="1" 
AR Path="/60088CAB/600AF199" Ref="#PWR023"  Part="1" 
AR Path="/60C45676/600AF199" Ref="#PWR012"  Part="1" 
F 0 "#PWR012" H 5950 5250 50  0001 C CNN
F 1 "GND" H 5955 5327 50  0000 C CNN
F 2 "" H 5950 5500 50  0001 C CNN
F 3 "" H 5950 5500 50  0001 C CNN
	1    5950 5500
	1    0    0    -1  
$EndComp
Wire Wire Line
	5950 5500 5950 5450
$Comp
L Device:R_Small R?
U 1 1 600AF1A0
P 10250 5050
AR Path="/600AF1A0" Ref="R?"  Part="1" 
AR Path="/5F2CBBC0/600AF1A0" Ref="R?"  Part="1" 
AR Path="/5F9CF0DB/600AF1A0" Ref="R?"  Part="1" 
AR Path="/60088CAB/600AF1A0" Ref="R2"  Part="1" 
AR Path="/60C45676/600AF1A0" Ref="R10"  Part="1" 
F 0 "R10" V 10200 4850 50  0000 C CNN
F 1 "1M" V 10250 5050 50  0000 C CNN
F 2 "Resistor_SMD:R_0603_1608Metric_Pad1.05x0.95mm_HandSolder" H 10250 5050 50  0001 C CNN
F 3 "~" H 10250 5050 50  0001 C CNN
	1    10250 5050
	0    1    1    0   
$EndComp
$Comp
L Device:C_Small C?
U 1 1 600AF1A6
P 10250 4850
AR Path="/600AF1A6" Ref="C?"  Part="1" 
AR Path="/5F2CBBC0/600AF1A6" Ref="C?"  Part="1" 
AR Path="/5F9CF0DB/600AF1A6" Ref="C?"  Part="1" 
AR Path="/60088CAB/600AF1A6" Ref="C11"  Part="1" 
AR Path="/60C45676/600AF1A6" Ref="C12"  Part="1" 
F 0 "C12" V 10300 4950 50  0000 L CNN
F 1 "100nF" V 10150 4750 50  0000 L CNN
F 2 "Capacitor_SMD:C_0603_1608Metric_Pad1.05x0.95mm_HandSolder" H 10250 4850 50  0001 C CNN
F 3 "~" H 10250 4850 50  0001 C CNN
	1    10250 4850
	0    -1   -1   0   
$EndComp
Wire Wire Line
	10350 5050 10400 5050
Wire Wire Line
	10400 5050 10400 4850
Wire Wire Line
	10400 4850 10350 4850
$Comp
L power:GND #PWR?
U 1 1 600AF1B2
P 10400 5200
AR Path="/600AF1B2" Ref="#PWR?"  Part="1" 
AR Path="/5F2CBBC0/600AF1B2" Ref="#PWR?"  Part="1" 
AR Path="/5F9CF0DB/600AF1B2" Ref="#PWR?"  Part="1" 
AR Path="/60088CAB/600AF1B2" Ref="#PWR08"  Part="1" 
AR Path="/60C45676/600AF1B2" Ref="#PWR034"  Part="1" 
F 0 "#PWR034" H 10400 4950 50  0001 C CNN
F 1 "GND" H 10405 5027 50  0000 C CNN
F 2 "" H 10400 5200 50  0001 C CNN
F 3 "" H 10400 5200 50  0001 C CNN
	1    10400 5200
	1    0    0    -1  
$EndComp
Wire Wire Line
	5950 4350 5950 4450
$Comp
L power:VBUS #PWR?
U 1 1 600AF1BA
P 5950 4350
AR Path="/600AF1BA" Ref="#PWR?"  Part="1" 
AR Path="/60088CAB/600AF1BA" Ref="#PWR022"  Part="1" 
AR Path="/60C45676/600AF1BA" Ref="#PWR011"  Part="1" 
F 0 "#PWR011" H 5950 4200 50  0001 C CNN
F 1 "VBUS" H 5965 4523 50  0000 C CNN
F 2 "" H 5950 4350 50  0001 C CNN
F 3 "" H 5950 4350 50  0001 C CNN
	1    5950 4350
	1    0    0    -1  
$EndComp
Text GLabel 10700 4250 2    50   Input ~ 0
USB+
Text GLabel 10700 4350 2    50   Input ~ 0
USB-
Text GLabel 5400 4650 1    50   Input ~ 0
USB-
Text GLabel 6500 5250 3    50   Input ~ 0
USB+
Text GLabel 10600 900  2    50   Input ~ 0
PA13|SWDIO
Text GLabel 10600 1000 2    50   Input ~ 0
PA14|SWCLK
Wire Wire Line
	10600 900  10500 900 
Wire Wire Line
	10600 1000 10500 1000
Wire Wire Line
	10600 1300 10500 1300
$Comp
L Connector_Generic:Conn_01x04 debugSerie?
U 1 1 600AF1DA
P 9310 4200
AR Path="/600AF1DA" Ref="debugSerie?"  Part="1" 
AR Path="/60088CAB/600AF1DA" Ref="DBG_Serial1"  Part="1" 
AR Path="/60C45676/600AF1DA" Ref="debugSerie1"  Part="1" 
F 0 "debugSerie1" H 8410 4350 50  0000 L CNN
F 1 "Conn_1x04_P2,54mm" H 8110 3900 50  0001 L CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_1x04_P2.54mm_Vertical" H 9310 4200 50  0001 C CNN
F 3 "~" H 9310 4200 50  0001 C CNN
	1    9310 4200
	1    0    0    -1  
$EndComp
Text GLabel 8760 4300 0    50   Input ~ 0
PA10|D0
Text GLabel 8760 4400 0    50   Input ~ 0
PA9|D1
Text GLabel 10600 1300 2    50   Input ~ 0
RESET
Text GLabel 7230 5500 0    50   Input ~ 0
PA0|A4
Text GLabel 7250 6110 0    50   Input ~ 0
PA5|A1
Text GLabel 8440 6150 0    50   Input ~ 0
PA2|A2
Text GLabel 8440 6050 0    50   Input ~ 0
PA3|A3
Wire Wire Line
	7230 5500 7380 5500
Wire Wire Line
	7400 6110 7250 6110
Wire Wire Line
	8430 5510 8580 5510
Wire Wire Line
	8590 6150 8440 6150
Wire Wire Line
	8440 6050 8590 6050
$Comp
L power:GND #PWR?
U 1 1 600AF22B
P 7190 5260
AR Path="/600AF22B" Ref="#PWR?"  Part="1" 
AR Path="/5F2CBBC0/600AF22B" Ref="#PWR?"  Part="1" 
AR Path="/5F9CF0DB/600AF22B" Ref="#PWR?"  Part="1" 
AR Path="/60088CAB/600AF22B" Ref="#PWR017"  Part="1" 
AR Path="/60C45676/600AF22B" Ref="#PWR019"  Part="1" 
F 0 "#PWR019" H 7190 5010 50  0001 C CNN
F 1 "GND" H 7195 5087 50  0000 C CNN
F 2 "" H 7190 5260 50  0001 C CNN
F 3 "" H 7190 5260 50  0001 C CNN
	1    7190 5260
	-1   0    0    1   
$EndComp
$Comp
L Connector_Generic:Conn_01x04 conI2C?
U 1 1 600AF244
P 7700 4200
AR Path="/600AF244" Ref="conI2C?"  Part="1" 
AR Path="/60088CAB/600AF244" Ref="conI2C1"  Part="1" 
AR Path="/60C45676/600AF244" Ref="conI2C1"  Part="1" 
F 0 "conI2C1" H 7300 4450 50  0000 L CNN
F 1 "conI2C" H 7780 4101 50  0001 L CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_1x04_P2.54mm_Vertical" H 7700 4200 50  0001 C CNN
F 3 "~" H 7700 4200 50  0001 C CNN
	1    7700 4200
	1    0    0    -1  
$EndComp
$Comp
L power:+3.3V #PWR?
U 1 1 600AF24A
P 7200 4000
AR Path="/600AF24A" Ref="#PWR?"  Part="1" 
AR Path="/5F2CBBC0/600AF24A" Ref="#PWR?"  Part="1" 
AR Path="/5F9CF0DB/600AF24A" Ref="#PWR?"  Part="1" 
AR Path="/60088CAB/600AF24A" Ref="#PWR014"  Part="1" 
AR Path="/60C45676/600AF24A" Ref="#PWR021"  Part="1" 
F 0 "#PWR021" H 7200 3850 50  0001 C CNN
F 1 "+3.3V" H 7215 4173 50  0000 C CNN
F 2 "" H 7200 4000 50  0001 C CNN
F 3 "" H 7200 4000 50  0001 C CNN
	1    7200 4000
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR?
U 1 1 600AF250
P 7000 4000
AR Path="/600AF250" Ref="#PWR?"  Part="1" 
AR Path="/5F2CBBC0/600AF250" Ref="#PWR?"  Part="1" 
AR Path="/5F9CF0DB/600AF250" Ref="#PWR?"  Part="1" 
AR Path="/60088CAB/600AF250" Ref="#PWR015"  Part="1" 
AR Path="/60C45676/600AF250" Ref="#PWR017"  Part="1" 
F 0 "#PWR017" H 7000 3750 50  0001 C CNN
F 1 "GND" H 7005 3827 50  0000 C CNN
F 2 "" H 7000 4000 50  0001 C CNN
F 3 "" H 7000 4000 50  0001 C CNN
	1    7000 4000
	-1   0    0    1   
$EndComp
Wire Wire Line
	7200 4100 7500 4100
Wire Wire Line
	7200 4000 7200 4100
Text GLabel 7350 4300 0    50   Input ~ 0
PB8|SCL
Text GLabel 7350 4400 0    50   Input ~ 0
PB9|SDA
Wire Wire Line
	7350 4300 7500 4300
Wire Wire Line
	7500 4400 7350 4400
$Comp
L Device:Battery BT?
U 1 1 600AF25E
P 7150 1150
AR Path="/600AF25E" Ref="BT?"  Part="1" 
AR Path="/60088CAB/600AF25E" Ref="BT1"  Part="1" 
AR Path="/60C45676/600AF25E" Ref="BT1"  Part="1" 
F 0 "BT1" H 7258 1196 50  0000 L CNN
F 1 "Battery" H 7258 1105 50  0000 L CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_1x02_P2.54mm_Vertical" V 7150 1210 50  0001 C CNN
F 3 "~" V 7150 1210 50  0001 C CNN
	1    7150 1150
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR?
U 1 1 600AF265
P 8500 1500
AR Path="/600AF265" Ref="#PWR?"  Part="1" 
AR Path="/5F2CBBC0/600AF265" Ref="#PWR?"  Part="1" 
AR Path="/5F9CF0DB/600AF265" Ref="#PWR?"  Part="1" 
AR Path="/60088CAB/600AF265" Ref="#PWR02"  Part="1" 
AR Path="/60C45676/600AF265" Ref="#PWR027"  Part="1" 
F 0 "#PWR027" H 8500 1250 50  0001 C CNN
F 1 "GND" H 8505 1327 50  0000 C CNN
F 2 "" H 8500 1500 50  0001 C CNN
F 3 "" H 8500 1500 50  0001 C CNN
	1    8500 1500
	1    0    0    -1  
$EndComp
Wire Wire Line
	8500 1500 8500 1450
$Comp
L power:+BATT #PWR?
U 1 1 600AF26C
P 7150 800
AR Path="/600AF26C" Ref="#PWR?"  Part="1" 
AR Path="/60088CAB/600AF26C" Ref="#PWR01"  Part="1" 
AR Path="/60C45676/600AF26C" Ref="#PWR020"  Part="1" 
F 0 "#PWR020" H 7150 650 50  0001 C CNN
F 1 "+BATT" H 7165 973 50  0000 C CNN
F 2 "" H 7150 800 50  0001 C CNN
F 3 "" H 7150 800 50  0001 C CNN
	1    7150 800 
	1    0    0    -1  
$EndComp
Wire Wire Line
	7150 1450 7150 1350
$Comp
L Device:C_Small C?
U 1 1 600AF281
P 7950 1250
AR Path="/600AF281" Ref="C?"  Part="1" 
AR Path="/5F2CBBC0/600AF281" Ref="C?"  Part="1" 
AR Path="/5F9CF0DB/600AF281" Ref="C?"  Part="1" 
AR Path="/60088CAB/600AF281" Ref="C1"  Part="1" 
AR Path="/60C45676/600AF281" Ref="C4"  Part="1" 
F 0 "C4" H 8042 1296 50  0000 L CNN
F 1 "100nF" H 8042 1205 50  0000 L CNN
F 2 "Capacitor_SMD:C_0603_1608Metric_Pad1.05x0.95mm_HandSolder" H 7950 1250 50  0001 C CNN
F 3 "~" H 7950 1250 50  0001 C CNN
	1    7950 1250
	1    0    0    -1  
$EndComp
$Comp
L Device:C_Small C?
U 1 1 600AF287
P 9100 1250
AR Path="/600AF287" Ref="C?"  Part="1" 
AR Path="/5F2CBBC0/600AF287" Ref="C?"  Part="1" 
AR Path="/5F9CF0DB/600AF287" Ref="C?"  Part="1" 
AR Path="/60088CAB/600AF287" Ref="C2"  Part="1" 
AR Path="/60C45676/600AF287" Ref="C10"  Part="1" 
F 0 "C10" H 9192 1296 50  0000 L CNN
F 1 "10uF" H 9192 1205 50  0000 L CNN
F 2 "Capacitor_SMD:C_0805_2012Metric_Pad1.15x1.40mm_HandSolder" H 9100 1250 50  0001 C CNN
F 3 "~" H 9100 1250 50  0001 C CNN
	1    9100 1250
	1    0    0    -1  
$EndComp
Wire Wire Line
	9100 1350 9100 1450
Wire Wire Line
	7150 1450 7950 1450
Connection ~ 8500 1450
Wire Wire Line
	7950 1350 7950 1450
Connection ~ 7950 1450
Wire Wire Line
	7950 1450 8500 1450
Wire Wire Line
	7950 1150 7950 900 
Connection ~ 7950 900 
Wire Wire Line
	7950 900  7600 900 
Wire Wire Line
	9100 1150 9100 900 
Connection ~ 9100 900 
$Comp
L power:+3.3V #PWR?
U 1 1 600AF2A0
P 9100 900
AR Path="/600AF2A0" Ref="#PWR?"  Part="1" 
AR Path="/60088CAB/600AF2A0" Ref="#PWR05"  Part="1" 
AR Path="/60C45676/600AF2A0" Ref="#PWR030"  Part="1" 
F 0 "#PWR030" H 9100 750 50  0001 C CNN
F 1 "+3.3V" H 9115 1073 50  0000 C CNN
F 2 "" H 9100 900 50  0001 C CNN
F 3 "" H 9100 900 50  0001 C CNN
	1    9100 900 
	1    0    0    -1  
$EndComp
Text Notes 500  2400 0    50   ~ 0
Conn_NonUsed\n
Text Notes 550  3900 0    50   ~ 0
Conn_NonUsed\n
Text Label 8810 4300 0    50   ~ 0
rx1
Text Label 8790 4400 0    50   ~ 0
tx1
$Comp
L power:VBUS #PWR?
U 1 1 5FF85CF5
P 6900 2500
AR Path="/5FF85CF5" Ref="#PWR?"  Part="1" 
AR Path="/60088CAB/5FF85CF5" Ref="#PWR028"  Part="1" 
AR Path="/60C45676/5FF85CF5" Ref="#PWR016"  Part="1" 
F 0 "#PWR016" H 6900 2350 50  0001 C CNN
F 1 "VBUS" H 6915 2673 50  0000 C CNN
F 2 "" H 6900 2500 50  0001 C CNN
F 3 "" H 6900 2500 50  0001 C CNN
	1    6900 2500
	1    0    0    -1  
$EndComp
$Comp
L Transistor_FET:BSS84 Q1
U 1 1 5FF88C0D
P 7200 2800
F 0 "Q1" V 7542 2800 50  0000 C CNN
F 1 "BSS84" V 7451 2800 50  0000 C CNN
F 2 "Package_TO_SOT_SMD:SOT-23" H 7400 2725 50  0001 L CIN
F 3 "http://assets.nexperia.com/documents/data-sheet/BSS84.pdf" H 7200 2800 50  0001 L CNN
	1    7200 2800
	0    -1   -1   0   
$EndComp
Wire Wire Line
	7000 2700 6900 2700
Wire Wire Line
	6900 2500 6900 2700
Wire Wire Line
	7200 3000 7200 3150
Wire Wire Line
	7200 3150 7550 3150
Wire Wire Line
	7550 3150 7550 3050
$Comp
L Device:R R7
U 1 1 5FFBAFD0
P 7550 2900
F 0 "R7" V 7450 2900 50  0000 C CNN
F 1 "47k" V 7550 2900 50  0000 C CNN
F 2 "Resistor_SMD:R_0603_1608Metric_Pad1.05x0.95mm_HandSolder" V 7480 2900 50  0001 C CNN
F 3 "~" H 7550 2900 50  0001 C CNN
	1    7550 2900
	-1   0    0    1   
$EndComp
Wire Wire Line
	7400 2700 7550 2700
Wire Wire Line
	7550 2700 7550 2750
Wire Wire Line
	7550 2700 7550 2450
Connection ~ 7550 2700
$Comp
L power:VCC #PWR023
U 1 1 5FFD55B8
P 7550 2450
F 0 "#PWR023" H 7550 2300 50  0001 C CNN
F 1 "VCC" H 7567 2623 50  0000 C CNN
F 2 "" H 7550 2450 50  0001 C CNN
F 3 "" H 7550 2450 50  0001 C CNN
	1    7550 2450
	1    0    0    -1  
$EndComp
Wire Notes Line width 10 style solid
	11200 500  11200 6500
Wire Wire Line
	11100 4050 11100 4000
Wire Wire Line
	9950 4850 10150 4850
Wire Wire Line
	9950 5050 10150 5050
Connection ~ 10400 4850
Wire Wire Line
	8760 4300 9110 4300
Wire Wire Line
	8760 4400 9110 4400
Text Notes 7050 6500 0    50   ~ 0
conn_Analogic\n
Text Notes 9530 4410 1    50   ~ 0
conn_DBG
Text Notes 7200 4850 0    50   ~ 0
conn_I2C
Text Notes 7050 3650 0    50   ~ 0
Supply adapted\n
Text Notes 5700 6500 0    50   ~ 0
ESD protection
Text Notes 9850 1900 0    50   ~ 0
CONN Programming JTAG\n
Text Notes 7600 1900 0    50   ~ 0
Power Supply\n
Text Notes 5800 1900 0    50   ~ 0
Boot Button\n
Wire Wire Line
	5400 4650 5400 4950
Wire Wire Line
	5400 4950 5450 4950
Wire Wire Line
	6500 5250 6500 4950
Wire Wire Line
	6450 4950 6500 4950
Text Notes 5800 3700 0    50   ~ 0
Reset Button\n
Text GLabel 3750 4050 2    50   Input ~ 0
ANT
Wire Wire Line
	3500 4050 3750 4050
$Comp
L power:+3.3V #PWR?
U 1 1 6060ED33
P 8150 2450
AR Path="/6060ED33" Ref="#PWR?"  Part="1" 
AR Path="/60088CAB/6060ED33" Ref="#PWR040"  Part="1" 
AR Path="/60C45676/6060ED33" Ref="#PWR026"  Part="1" 
F 0 "#PWR026" H 8150 2300 50  0001 C CNN
F 1 "+3.3V" H 8165 2623 50  0000 C CNN
F 2 "" H 8150 2450 50  0001 C CNN
F 3 "" H 8150 2450 50  0001 C CNN
	1    8150 2450
	1    0    0    -1  
$EndComp
Wire Wire Line
	8150 2450 8150 2550
Wire Wire Line
	8150 2550 8350 2550
Wire Wire Line
	8350 2550 8350 2600
Connection ~ 8350 2550
Wire Wire Line
	8350 2550 8500 2550
Wire Wire Line
	8500 2550 8500 2600
Connection ~ 8500 2550
Wire Wire Line
	8500 2550 8650 2550
Wire Wire Line
	8650 2550 8650 2600
Connection ~ 8650 2550
Wire Wire Line
	8650 2550 8800 2550
Wire Wire Line
	8800 2550 8800 2600
Connection ~ 8800 2550
Wire Wire Line
	8800 2550 8950 2550
Wire Wire Line
	8950 2550 8950 2600
Connection ~ 8950 2550
Wire Wire Line
	8950 2550 9100 2550
Wire Wire Line
	9100 2550 9100 2600
Text Notes 8350 3650 0    50   ~ 0
Supply Decoupling
Text Notes 8200 2200 0    100  Italic 20
 Close to uC
Text GLabel 5200 6900 0    50   Input ~ 0
ANT
Wire Notes Line
	6700 7600 850  7600
Text Notes 5300 7750 0    50   ~ 0
Antena Output\n
Wire Notes Line width 10 style solid rgb(21, 194, 21)
	5250 500  5250 6500
Wire Notes Line width 10 style solid
	11200 500  9550 500 
Wire Notes Line width 10 style solid
	9550 500  9550 3700
Wire Notes Line width 10 style solid
	9550 3700 6700 3700
Wire Notes Line width 10 style solid
	6700 3700 6700 6500
Wire Notes Line width 10 style solid
	6700 6500 11200 6500
Wire Notes Line
	9550 6500 9600 6500
Wire Notes Line width 10 style solid rgb(194, 71, 33)
	6700 500  6700 3650
Wire Notes Line
	9550 3700 11200 3700
Wire Notes Line width 10 style solid rgb(29, 194, 17)
	6650 500  5250 500 
Wire Notes Line width 10 style solid rgb(194, 41, 18)
	9500 500  9500 3650
Wire Notes Line width 10 style solid rgb(16, 194, 14)
	5250 6500 6650 6500
Wire Notes Line width 10 style solid rgb(21, 194, 21)
	6650 500  6650 6500
Wire Notes Line width 10 style solid rgb(194, 28, 23)
	9500 500  6700 500 
Wire Notes Line width 10 style solid rgb(194, 28, 23)
	9500 3650 6700 3650
Wire Notes Line
	11200 3550 9550 3550
Wire Notes Line
	6700 3500 9500 3500
Wire Notes Line
	9500 3500 9500 3450
Wire Notes Line
	6650 3550 5250 3550
Wire Notes Line
	6650 3700 5250 3700
Wire Notes Line
	5250 1900 6650 1900
Wire Notes Line
	5250 1750 6650 1750
Wire Notes Line
	6700 1750 9500 1750
Wire Notes Line
	6700 1900 9500 1900
Wire Notes Line
	9550 1750 11200 1750
Wire Notes Line
	9550 1900 11200 1900
Wire Notes Line
	7950 1900 7950 3650
Wire Notes Line
	5250 6400 6650 6400
Wire Notes Line
	6700 6400 11200 6400
Wire Notes Line width 10 style solid rgb(194, 80, 194)
	850  6550 6700 6550
Wire Notes Line width 10 style solid rgb(194, 80, 194)
	6700 6550 6700 7750
Wire Notes Line width 10 style solid rgb(194, 80, 194)
	850  6550 850  7750
Wire Notes Line width 10 style solid rgb(194, 80, 194)
	850  7750 6700 7750
Text GLabel 3450 7050 0    50   Input ~ 0
PB13|D13
$Comp
L Device:R_Small R?
U 1 1 60E7650B
P 3600 7050
AR Path="/60E7650B" Ref="R?"  Part="1" 
AR Path="/60088CAB/60E7650B" Ref="R1"  Part="1" 
AR Path="/60C45676/60E7650B" Ref="R1"  Part="1" 
F 0 "R1" V 3700 7000 50  0000 L CNN
F 1 "470" V 3500 7000 50  0000 L CNN
F 2 "Resistor_SMD:R_0805_2012Metric_Pad1.15x1.40mm_HandSolder" H 3600 7050 50  0001 C CNN
F 3 "~" H 3600 7050 50  0001 C CNN
	1    3600 7050
	0    -1   -1   0   
$EndComp
$Comp
L power:VCC #PWR025
U 1 1 60EA374A
P 7950 800
F 0 "#PWR025" H 7950 650 50  0001 C CNN
F 1 "VCC" H 7967 973 50  0000 C CNN
F 2 "" H 7950 800 50  0001 C CNN
F 3 "" H 7950 800 50  0001 C CNN
	1    7950 800 
	1    0    0    -1  
$EndComp
$Comp
L Device:D D2
U 1 1 60EA5164
P 7450 900
F 0 "D2" H 7450 684 50  0000 C CNN
F 1 "schottky" H 7450 775 50  0000 C CNN
F 2 "Diode_SMD:D_SOD-123" H 7450 900 50  0001 C CNN
F 3 "~" H 7450 900 50  0001 C CNN
	1    7450 900 
	-1   0    0    1   
$EndComp
Wire Wire Line
	7150 800  7150 900 
Wire Wire Line
	7300 900  7150 900 
Connection ~ 7150 900 
Wire Wire Line
	7150 900  7150 950 
Wire Wire Line
	7950 800  7950 900 
$Comp
L power:GND #PWR?
U 1 1 60FEA05F
P 4200 7050
AR Path="/60FEA05F" Ref="#PWR?"  Part="1" 
AR Path="/5F2CBBC0/60FEA05F" Ref="#PWR?"  Part="1" 
AR Path="/5F9CF0DB/60FEA05F" Ref="#PWR?"  Part="1" 
AR Path="/60088CAB/60FEA05F" Ref="#PWR037"  Part="1" 
AR Path="/60C45676/60FEA05F" Ref="#PWR06"  Part="1" 
F 0 "#PWR06" H 4200 6800 50  0001 C CNN
F 1 "GND" H 4205 6877 50  0000 C CNN
F 2 "" H 4200 7050 50  0001 C CNN
F 3 "" H 4200 7050 50  0001 C CNN
	1    4200 7050
	0    -1   -1   0   
$EndComp
Wire Wire Line
	5600 7200 5600 7250
Wire Wire Line
	3500 7050 3450 7050
Wire Wire Line
	4050 7050 4200 7050
$Comp
L Device:LED D1
U 1 1 6117EE90
P 3900 7050
F 0 "D1" H 3893 6795 50  0000 C CNN
F 1 "LED" H 3893 6886 50  0000 C CNN
F 2 "LED_SMD:LED_0805_2012Metric_Pad1.15x1.40mm_HandSolder" H 3900 7050 50  0001 C CNN
F 3 "~" H 3900 7050 50  0001 C CNN
	1    3900 7050
	-1   0    0    1   
$EndComp
Wire Wire Line
	3700 7050 3750 7050
Wire Notes Line
	4700 6550 4700 7750
Text Notes 3450 7750 0    50   ~ 0
USR LED
Wire Notes Line
	2800 6550 2800 7750
Wire Wire Line
	8500 1450 8500 1200
Wire Wire Line
	8500 1450 9100 1450
Wire Wire Line
	6000 1300 6000 1500
Wire Wire Line
	5500 800  5500 850 
Wire Wire Line
	5500 850  5550 850 
Wire Wire Line
	5950 850  6000 850 
Connection ~ 6000 850 
$Comp
L power:+3.3V #PWR?
U 1 1 5FC22014
P 7900 3950
AR Path="/5FC22014" Ref="#PWR?"  Part="1" 
AR Path="/5F2CBBC0/5FC22014" Ref="#PWR?"  Part="1" 
AR Path="/5F9CF0DB/5FC22014" Ref="#PWR?"  Part="1" 
AR Path="/60088CAB/5FC22014" Ref="#PWR0102"  Part="1" 
AR Path="/60C45676/5FC22014" Ref="#PWR024"  Part="1" 
F 0 "#PWR024" H 7900 3800 50  0001 C CNN
F 1 "+3.3V" H 7915 4123 50  0000 C CNN
F 2 "" H 7900 3950 50  0001 C CNN
F 3 "" H 7900 3950 50  0001 C CNN
	1    7900 3950
	1    0    0    -1  
$EndComp
$Comp
L Device:R_Small R?
U 1 1 5FC23DFA
P 7900 4200
AR Path="/5FC23DFA" Ref="R?"  Part="1" 
AR Path="/60088CAB/5FC23DFA" Ref="R14"  Part="1" 
AR Path="/60C45676/5FC23DFA" Ref="R8"  Part="1" 
F 0 "R8" H 7950 4400 50  0000 L CNN
F 1 "4,7k" V 7900 4150 50  0000 L CNN
F 2 "Resistor_SMD:R_0805_2012Metric_Pad1.15x1.40mm_HandSolder" H 7900 4200 50  0001 C CNN
F 3 "~" H 7900 4200 50  0001 C CNN
	1    7900 4200
	1    0    0    -1  
$EndComp
$Comp
L Device:R_Small R?
U 1 1 5FC24A1B
P 8050 4200
AR Path="/5FC24A1B" Ref="R?"  Part="1" 
AR Path="/60088CAB/5FC24A1B" Ref="R15"  Part="1" 
AR Path="/60C45676/5FC24A1B" Ref="R9"  Part="1" 
F 0 "R9" H 8050 4400 50  0000 L CNN
F 1 "4,7k" V 8050 4150 50  0000 L CNN
F 2 "Resistor_SMD:R_0805_2012Metric_Pad1.15x1.40mm_HandSolder" H 8050 4200 50  0001 C CNN
F 3 "~" H 8050 4200 50  0001 C CNN
	1    8050 4200
	1    0    0    -1  
$EndComp
Wire Wire Line
	7900 4100 7900 4050
Wire Wire Line
	8050 4050 8050 4100
Wire Wire Line
	7900 3950 7900 4050
Connection ~ 7900 4050
Wire Wire Line
	7900 4050 8050 4050
Text GLabel 7900 4350 3    50   Input ~ 0
PB8|SCL
Text GLabel 8050 4350 3    50   Input ~ 0
PB9|SDA
Wire Wire Line
	7900 4300 7900 4350
Wire Wire Line
	8050 4300 8050 4350
$Comp
L Device:R_Small R?
U 1 1 5FC25581
P 4400 5450
AR Path="/5FC25581" Ref="R?"  Part="1" 
AR Path="/60088CAB/5FC25581" Ref="R16"  Part="1" 
AR Path="/60C45676/5FC25581" Ref="R2"  Part="1" 
F 0 "R2" H 4450 5650 50  0000 L CNN
F 1 "27k" V 4400 5400 50  0000 L CNN
F 2 "Resistor_SMD:R_0805_2012Metric_Pad1.15x1.40mm_HandSolder" H 4400 5450 50  0001 C CNN
F 3 "~" H 4400 5450 50  0001 C CNN
	1    4400 5450
	1    0    0    -1  
$EndComp
$Comp
L Device:R_Small R?
U 1 1 5FC25587
P 4400 5850
AR Path="/5FC25587" Ref="R?"  Part="1" 
AR Path="/60088CAB/5FC25587" Ref="R17"  Part="1" 
AR Path="/60C45676/5FC25587" Ref="R3"  Part="1" 
F 0 "R3" H 4400 6050 50  0000 L CNN
F 1 "100k" V 4400 5800 50  0000 L CNN
F 2 "Resistor_SMD:R_0805_2012Metric_Pad1.15x1.40mm_HandSolder" H 4400 5850 50  0001 C CNN
F 3 "~" H 4400 5850 50  0001 C CNN
	1    4400 5850
	1    0    0    -1  
$EndComp
$Comp
L power:+3.3V #PWR?
U 1 1 5FC4575A
P 2800 1100
AR Path="/5FC4575A" Ref="#PWR?"  Part="1" 
AR Path="/60088CAB/5FC4575A" Ref="#PWR024"  Part="1" 
AR Path="/60C45676/5FC4575A" Ref="#PWR03"  Part="1" 
F 0 "#PWR03" H 2800 950 50  0001 C CNN
F 1 "+3.3V" H 2815 1273 50  0000 C CNN
F 2 "" H 2800 1100 50  0001 C CNN
F 3 "" H 2800 1100 50  0001 C CNN
	1    2800 1100
	1    0    0    -1  
$EndComp
$Comp
L power:+BATT #PWR?
U 1 1 5FC45B2D
P 4400 5250
AR Path="/5FC45B2D" Ref="#PWR?"  Part="1" 
AR Path="/60088CAB/5FC45B2D" Ref="#PWR025"  Part="1" 
AR Path="/60C45676/5FC45B2D" Ref="#PWR07"  Part="1" 
F 0 "#PWR07" H 4400 5100 50  0001 C CNN
F 1 "+BATT" H 4415 5423 50  0000 C CNN
F 2 "" H 4400 5250 50  0001 C CNN
F 3 "" H 4400 5250 50  0001 C CNN
	1    4400 5250
	1    0    0    -1  
$EndComp
Wire Wire Line
	4400 5250 4400 5350
Wire Wire Line
	4400 5550 4400 5650
$Comp
L power:GND #PWR?
U 1 1 5FC67C29
P 4400 6050
AR Path="/5FC67C29" Ref="#PWR?"  Part="1" 
AR Path="/5F2CBBC0/5FC67C29" Ref="#PWR?"  Part="1" 
AR Path="/5F9CF0DB/5FC67C29" Ref="#PWR?"  Part="1" 
AR Path="/60088CAB/5FC67C29" Ref="#PWR030"  Part="1" 
AR Path="/60C45676/5FC67C29" Ref="#PWR08"  Part="1" 
F 0 "#PWR08" H 4400 5800 50  0001 C CNN
F 1 "GND" H 4405 5877 50  0000 C CNN
F 2 "" H 4400 6050 50  0001 C CNN
F 3 "" H 4400 6050 50  0001 C CNN
	1    4400 6050
	1    0    0    -1  
$EndComp
Wire Wire Line
	4400 5950 4400 6050
Wire Wire Line
	4400 5650 4200 5650
Connection ~ 4400 5650
Wire Wire Line
	4400 5650 4400 5750
Text Label 4200 5650 0    50   ~ 0
Vref
Text Label 2700 1050 1    50   ~ 0
Vref
$Comp
L power:PWR_FLAG #FLG?
U 1 1 5FC8A54B
P 4200 5650
AR Path="/5FC8A54B" Ref="#FLG?"  Part="1" 
AR Path="/60088CAB/5FC8A54B" Ref="#FLG01"  Part="1" 
AR Path="/60C45676/5FC8A54B" Ref="#FLG01"  Part="1" 
F 0 "#FLG01" H 4200 5725 50  0001 C CNN
F 1 "PWR_FLAG" V 4200 6000 50  0000 C CNN
F 2 "" H 4200 5650 50  0001 C CNN
F 3 "~" H 4200 5650 50  0001 C CNN
	1    4200 5650
	-1   0    0    1   
$EndComp
Wire Wire Line
	2500 1100 2600 1100
Connection ~ 2500 1100
Wire Wire Line
	2700 1050 2700 1250
$Comp
L power:VCC #PWR04
U 1 1 5FC8574A
P 2950 5250
F 0 "#PWR04" H 2950 5100 50  0001 C CNN
F 1 "VCC" H 2967 5423 50  0000 C CNN
F 2 "" H 2950 5250 50  0001 C CNN
F 3 "" H 2950 5250 50  0001 C CNN
	1    2950 5250
	1    0    0    -1  
$EndComp
Wire Wire Line
	2950 5350 2950 5300
Wire Wire Line
	2950 5300 3050 5300
Wire Wire Line
	3050 5300 3050 5350
Connection ~ 2950 5300
Wire Wire Line
	2950 5300 2950 5250
Wire Wire Line
	2950 5950 2950 6000
Wire Wire Line
	2950 6000 3050 6000
Wire Wire Line
	3050 6000 3050 5950
Connection ~ 2950 6000
Wire Wire Line
	2950 6150 3100 6150
Wire Wire Line
	2950 6000 2950 6150
$Comp
L power:GND #PWR?
U 1 1 5FCF041F
P 2950 6300
AR Path="/5FCF041F" Ref="#PWR?"  Part="1" 
AR Path="/5F2CBBC0/5FCF041F" Ref="#PWR?"  Part="1" 
AR Path="/5F9CF0DB/5FCF041F" Ref="#PWR?"  Part="1" 
AR Path="/60088CAB/5FCF041F" Ref="#PWR033"  Part="1" 
AR Path="/60C45676/5FCF041F" Ref="#PWR05"  Part="1" 
F 0 "#PWR05" H 2950 6050 50  0001 C CNN
F 1 "GND" H 2955 6127 50  0000 C CNN
F 2 "" H 2950 6300 50  0001 C CNN
F 3 "" H 2950 6300 50  0001 C CNN
	1    2950 6300
	1    0    0    -1  
$EndComp
Wire Wire Line
	2950 6300 2950 6250
Wire Wire Line
	2950 6250 3100 6250
$Comp
L Power_Protection:PRTR5V0U2X PRTR5V?
U 1 1 600AF193
P 5950 4950
AR Path="/600AF193" Ref="PRTR5V?"  Part="1" 
AR Path="/60088CAB/600AF193" Ref="PRTR5V1"  Part="1" 
AR Path="/60C45676/600AF193" Ref="PRTR5V1"  Part="1" 
F 0 "PRTR5V1" H 5600 4400 50  0000 L CNN
F 1 "PRTR5V0U2X" H 5400 4500 50  0000 L CNN
F 2 "Package_TO_SOT_SMD:SOT-143" H 6010 4950 50  0001 C CNN
F 3 "https://assets.nexperia.com/documents/data-sheet/PRTR5V0U2X.pdf" H 6010 4950 50  0001 C CNN
	1    5950 4950
	1    0    0    -1  
$EndComp
$Comp
L Switch:SW_DIP_x02 SW1
U 1 1 5FC7B947
P 2950 5650
F 0 "SW1" V 2904 5880 50  0000 L CNN
F 1 "SW_DIP_x02" V 2995 5880 50  0000 L CNN
F 2 "_Conectores:Conector_CTS_ssgc" H 2950 5650 50  0001 C CNN
F 3 "~" H 2950 5650 50  0001 C CNN
	1    2950 5650
	0    1    1    0   
$EndComp
$Comp
L Connector_Generic:Conn_01x02 J1
U 1 1 5FCD663B
P 3300 6150
F 0 "J1" H 3380 6142 50  0000 L CNN
F 1 "VCC_CON" H 3380 6051 50  0000 L CNN
F 2 "TerminalBlock:TerminalBlock_bornier-2_P5.08mm" H 3300 6150 50  0001 C CNN
F 3 "~" H 3300 6150 50  0001 C CNN
	1    3300 6150
	1    0    0    -1  
$EndComp
$Comp
L Device:Fuse F1
U 1 1 5FDD67BD
P 10950 4050
F 0 "F1" V 10845 4050 50  0000 C CNN
F 1 "RF1335-000" V 10754 4050 50  0001 C CNN
F 2 "Fuse:Fuse_1812_4532Metric_Pad1.30x3.40mm_HandSolder" V 10845 4050 50  0001 C CNN
F 3 "https://docs.rs-online.com/3432/0900766b81637c03.pdf" H 10950 4050 50  0001 C CNN
	1    10950 4050
	0    1    1    0   
$EndComp
$Comp
L power:PWR_FLAG #FLG03
U 1 1 5FE7CCA2
P 10800 6050
F 0 "#FLG03" H 10800 6125 50  0001 C CNN
F 1 "PWR_FLAG" H 10800 6223 50  0000 C CNN
F 2 "" H 10800 6050 50  0001 C CNN
F 3 "~" H 10800 6050 50  0001 C CNN
	1    10800 6050
	-1   0    0    1   
$EndComp
$Comp
L power:VBUS #PWR?
U 1 1 5FE7DC74
P 10800 6050
AR Path="/5FE7DC74" Ref="#PWR?"  Part="1" 
AR Path="/60088CAB/5FE7DC74" Ref="#PWR034"  Part="1" 
AR Path="/60C45676/5FE7DC74" Ref="#PWR035"  Part="1" 
F 0 "#PWR035" H 10800 5900 50  0001 C CNN
F 1 "VBUS" H 10815 6223 50  0000 C CNN
F 2 "" H 10800 6050 50  0001 C CNN
F 3 "" H 10800 6050 50  0001 C CNN
	1    10800 6050
	1    0    0    -1  
$EndComp
Wire Wire Line
	7000 4000 7000 4200
Wire Wire Line
	7000 4200 7500 4200
Wire Wire Line
	10400 5050 10400 5200
Connection ~ 10400 5050
Wire Wire Line
	10400 4650 10400 4850
Wire Wire Line
	9950 4700 9950 4850
Connection ~ 9950 4850
Wire Wire Line
	9950 4850 9950 5050
$Comp
L power:VBUS #PWR?
U 1 1 600AF1C2
P 11100 4000
AR Path="/600AF1C2" Ref="#PWR?"  Part="1" 
AR Path="/60088CAB/600AF1C2" Ref="#PWR011"  Part="1" 
AR Path="/60C45676/600AF1C2" Ref="#PWR036"  Part="1" 
F 0 "#PWR036" H 11100 3850 50  0001 C CNN
F 1 "VBUS" H 11115 4173 50  0000 C CNN
F 2 "" H 11100 4000 50  0001 C CNN
F 3 "" H 11100 4000 50  0001 C CNN
	1    11100 4000
	1    0    0    -1  
$EndComp
NoConn ~ 3500 1850
NoConn ~ 3500 2150
NoConn ~ 3500 2950
NoConn ~ 3500 3050
NoConn ~ 3500 3150
NoConn ~ 3500 3350
NoConn ~ 3500 3450
NoConn ~ 3500 3550
NoConn ~ 3500 3650
NoConn ~ 3500 3750
NoConn ~ 3500 3850
NoConn ~ 1700 3550
NoConn ~ 1700 3650
NoConn ~ 1700 3350
NoConn ~ 1700 3850
$Comp
L Connector_Generic:Conn_01x03 J5
U 1 1 60F1B0A1
P 7580 5400
F 0 "J5" H 7660 5442 50  0000 L CNN
F 1 "CO2Sensor" H 7660 5351 50  0000 L CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_1x03_P2.54mm_Vertical" H 7580 5400 50  0001 C CNN
F 3 "~" H 7580 5400 50  0001 C CNN
	1    7580 5400
	1    0    0    -1  
$EndComp
$Comp
L Connector_Generic:Conn_01x03 J6
U 1 1 60F1C234
P 7600 6010
F 0 "J6" H 7680 6052 50  0000 L CNN
F 1 "NH3Sensor" H 7680 5961 50  0000 L CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_1x03_P2.54mm_Vertical" H 7600 6010 50  0001 C CNN
F 3 "~" H 7600 6010 50  0001 C CNN
	1    7600 6010
	1    0    0    -1  
$EndComp
$Comp
L Connector_Generic:Conn_01x03 J7
U 1 1 60F45801
P 8780 5410
F 0 "J7" H 8860 5452 50  0000 L CNN
F 1 "MetanoSensor" H 8860 5361 50  0000 L CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_1x03_P2.54mm_Vertical" H 8780 5410 50  0001 C CNN
F 3 "~" H 8780 5410 50  0001 C CNN
	1    8780 5410
	1    0    0    -1  
$EndComp
$Comp
L Connector_Generic:Conn_01x04 J8
U 1 1 60F79D74
P 8790 5950
F 0 "J8" H 8870 5942 50  0000 L CNN
F 1 "AnalConn" H 8870 5851 50  0000 L CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_1x04_P2.54mm_Vertical" H 8790 5950 50  0001 C CNN
F 3 "~" H 8790 5950 50  0001 C CNN
	1    8790 5950
	1    0    0    -1  
$EndComp
Text GLabel 3750 2550 2    50   Input ~ 0
PB13|D13
Wire Wire Line
	3750 2550 3500 2550
NoConn ~ 1700 2350
$Comp
L power:VCC #PWR018
U 1 1 60D2D4DA
P 7340 5250
F 0 "#PWR018" H 7340 5100 50  0001 C CNN
F 1 "VCC" H 7357 5423 50  0000 C CNN
F 2 "" H 7340 5250 50  0001 C CNN
F 3 "" H 7340 5250 50  0001 C CNN
	1    7340 5250
	1    0    0    -1  
$EndComp
$Comp
L Connector:USB_B_Micro J3
U 1 1 60DEFA4B
P 10400 4250
F 0 "J3" H 10457 4717 50  0000 C CNN
F 1 "USB_B_Micro" H 10457 4626 50  0000 C CNN
F 2 "Connector_USB:USB_Mini-B_Tensility_54-00023_Vertical" H 10550 4200 50  0001 C CNN
F 3 "~" H 10550 4200 50  0001 C CNN
	1    10400 4250
	1    0    0    -1  
$EndComp
NoConn ~ 10700 4450
Wire Wire Line
	9950 4700 10300 4700
Wire Wire Line
	10300 4650 10300 4700
Wire Wire Line
	10700 4050 10800 4050
$Comp
L Mechanical:MountingHole H1
U 1 1 60E67986
P 1350 7040
F 0 "H1" H 1450 7086 50  0000 L CNN
F 1 "MountingHole" H 1450 6995 50  0000 L CNN
F 2 "MountingHole:MountingHole_3.2mm_M3_Pad_Via" H 1350 7040 50  0001 C CNN
F 3 "~" H 1350 7040 50  0001 C CNN
	1    1350 7040
	1    0    0    -1  
$EndComp
$Comp
L Mechanical:MountingHole H2
U 1 1 60E684F7
P 1360 7250
F 0 "H2" H 1460 7296 50  0000 L CNN
F 1 "MountingHole" H 1460 7205 50  0000 L CNN
F 2 "MountingHole:MountingHole_3.2mm_M3_Pad_Via" H 1360 7250 50  0001 C CNN
F 3 "~" H 1360 7250 50  0001 C CNN
	1    1360 7250
	1    0    0    -1  
$EndComp
$Comp
L Connector_Generic:Conn_01x02 J9
U 1 1 60CA9A25
P 1400 5450
F 0 "J9" H 1480 5442 50  0000 L CNN
F 1 "Conn_01x02" H 1480 5351 50  0000 L CNN
F 2 "_Conectores:bat_802535_35x35x8mm_Li-Po" H 1400 5450 50  0001 C CNN
F 3 "~" H 1400 5450 50  0001 C CNN
	1    1400 5450
	1    0    0    -1  
$EndComp
$Comp
L power:+BATT #PWR?
U 1 1 60CAEE99
P 1050 5250
AR Path="/60CAEE99" Ref="#PWR?"  Part="1" 
AR Path="/60088CAB/60CAEE99" Ref="#PWR?"  Part="1" 
AR Path="/60C45676/60CAEE99" Ref="#PWR022"  Part="1" 
F 0 "#PWR022" H 1050 5100 50  0001 C CNN
F 1 "+BATT" H 1065 5423 50  0000 C CNN
F 2 "" H 1050 5250 50  0001 C CNN
F 3 "" H 1050 5250 50  0001 C CNN
	1    1050 5250
	1    0    0    -1  
$EndComp
Wire Wire Line
	1050 5250 1050 5450
$Comp
L power:GND #PWR?
U 1 1 60CBC0B8
P 1050 5600
AR Path="/60CBC0B8" Ref="#PWR?"  Part="1" 
AR Path="/5F2CBBC0/60CBC0B8" Ref="#PWR?"  Part="1" 
AR Path="/5F9CF0DB/60CBC0B8" Ref="#PWR?"  Part="1" 
AR Path="/60088CAB/60CBC0B8" Ref="#PWR?"  Part="1" 
AR Path="/60C45676/60CBC0B8" Ref="#PWR038"  Part="1" 
F 0 "#PWR038" H 1050 5350 50  0001 C CNN
F 1 "GND" H 1055 5427 50  0000 C CNN
F 2 "" H 1050 5600 50  0001 C CNN
F 3 "" H 1050 5600 50  0001 C CNN
	1    1050 5600
	1    0    0    -1  
$EndComp
Wire Wire Line
	1050 5600 1050 5550
Wire Wire Line
	1050 5550 1200 5550
Wire Wire Line
	1050 5450 1200 5450
$Comp
L Connector_Generic:Conn_01x05 J11
U 1 1 60D3B94C
P 9870 6000
F 0 "J11" H 9950 6042 50  0000 L CNN
F 1 "MICS 6814" H 9950 5951 50  0000 L CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_1x05_P2.54mm_Vertical" H 9870 6000 50  0001 C CNN
F 3 "~" H 9870 6000 50  0001 C CNN
	1    9870 6000
	1    0    0    -1  
$EndComp
Text GLabel 9670 6000 0    50   Input ~ 0
PA5|A1
Text GLabel 9670 6100 0    50   Input ~ 0
PA4|A0
Text GLabel 9670 6200 0    50   Input ~ 0
PA3|A3
Text GLabel 8430 5510 0    50   Input ~ 0
PA4|A0
Text GLabel 3750 2450 2    50   Input ~ 0
PB12|D10
Text GLabel 2950 6000 0    50   Input ~ 0
VCC_COM
Wire Wire Line
	3500 2450 3750 2450
$Comp
L Connector_Generic:Conn_01x02 J16
U 1 1 6100093B
P 10600 2650
F 0 "J16" H 10680 2642 50  0000 L CNN
F 1 "Fan" H 10680 2551 50  0000 L CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_1x02_P2.54mm_Vertical" H 10600 2650 50  0001 C CNN
F 3 "~" H 10600 2650 50  0001 C CNN
	1    10600 2650
	1    0    0    -1  
$EndComp
$Comp
L power:VCC #PWR064
U 1 1 61006235
P 10300 2600
F 0 "#PWR064" H 10300 2450 50  0001 C CNN
F 1 "VCC" H 10317 2773 50  0000 C CNN
F 2 "" H 10300 2600 50  0001 C CNN
F 3 "" H 10300 2600 50  0001 C CNN
	1    10300 2600
	1    0    0    -1  
$EndComp
Wire Wire Line
	10300 2600 10300 2650
Wire Wire Line
	10300 2650 10400 2650
$Comp
L power:GND #PWR?
U 1 1 61012C8D
P 10300 2800
AR Path="/61012C8D" Ref="#PWR?"  Part="1" 
AR Path="/5F2CBBC0/61012C8D" Ref="#PWR?"  Part="1" 
AR Path="/5F9CF0DB/61012C8D" Ref="#PWR?"  Part="1" 
AR Path="/60088CAB/61012C8D" Ref="#PWR?"  Part="1" 
AR Path="/60C45676/61012C8D" Ref="#PWR065"  Part="1" 
F 0 "#PWR065" H 10300 2550 50  0001 C CNN
F 1 "GND" H 10305 2627 50  0000 C CNN
F 2 "" H 10300 2800 50  0001 C CNN
F 3 "" H 10300 2800 50  0001 C CNN
	1    10300 2800
	1    0    0    -1  
$EndComp
Wire Wire Line
	10300 2800 10300 2750
Wire Wire Line
	10300 2750 10400 2750
Wire Wire Line
	7340 5250 7340 5300
Wire Wire Line
	7340 5300 7380 5300
Wire Wire Line
	7190 5260 7190 5400
Wire Wire Line
	7190 5400 7380 5400
$Comp
L power:GND #PWR?
U 1 1 60D0A1B2
P 7210 5870
AR Path="/60D0A1B2" Ref="#PWR?"  Part="1" 
AR Path="/5F2CBBC0/60D0A1B2" Ref="#PWR?"  Part="1" 
AR Path="/5F9CF0DB/60D0A1B2" Ref="#PWR?"  Part="1" 
AR Path="/60088CAB/60D0A1B2" Ref="#PWR?"  Part="1" 
AR Path="/60C45676/60D0A1B2" Ref="#PWR028"  Part="1" 
F 0 "#PWR028" H 7210 5620 50  0001 C CNN
F 1 "GND" H 7215 5697 50  0000 C CNN
F 2 "" H 7210 5870 50  0001 C CNN
F 3 "" H 7210 5870 50  0001 C CNN
	1    7210 5870
	-1   0    0    1   
$EndComp
$Comp
L power:VCC #PWR029
U 1 1 60D0A1B8
P 7360 5860
F 0 "#PWR029" H 7360 5710 50  0001 C CNN
F 1 "VCC" H 7377 6033 50  0000 C CNN
F 2 "" H 7360 5860 50  0001 C CNN
F 3 "" H 7360 5860 50  0001 C CNN
	1    7360 5860
	1    0    0    -1  
$EndComp
Wire Wire Line
	7360 5860 7360 5910
Wire Wire Line
	7360 5910 7400 5910
Wire Wire Line
	7210 5870 7210 6010
Wire Wire Line
	7210 6010 7400 6010
$Comp
L power:GND #PWR?
U 1 1 60D386B6
P 8390 5270
AR Path="/60D386B6" Ref="#PWR?"  Part="1" 
AR Path="/5F2CBBC0/60D386B6" Ref="#PWR?"  Part="1" 
AR Path="/5F9CF0DB/60D386B6" Ref="#PWR?"  Part="1" 
AR Path="/60088CAB/60D386B6" Ref="#PWR?"  Part="1" 
AR Path="/60C45676/60D386B6" Ref="#PWR037"  Part="1" 
F 0 "#PWR037" H 8390 5020 50  0001 C CNN
F 1 "GND" H 8395 5097 50  0000 C CNN
F 2 "" H 8390 5270 50  0001 C CNN
F 3 "" H 8390 5270 50  0001 C CNN
	1    8390 5270
	-1   0    0    1   
$EndComp
$Comp
L power:VCC #PWR040
U 1 1 60D386BC
P 8540 5260
F 0 "#PWR040" H 8540 5110 50  0001 C CNN
F 1 "VCC" H 8557 5433 50  0000 C CNN
F 2 "" H 8540 5260 50  0001 C CNN
F 3 "" H 8540 5260 50  0001 C CNN
	1    8540 5260
	1    0    0    -1  
$EndComp
Wire Wire Line
	8540 5260 8540 5310
Wire Wire Line
	8540 5310 8580 5310
Wire Wire Line
	8390 5270 8390 5410
Wire Wire Line
	8390 5410 8580 5410
$Comp
L power:GND #PWR?
U 1 1 60D44292
P 8400 5810
AR Path="/60D44292" Ref="#PWR?"  Part="1" 
AR Path="/5F2CBBC0/60D44292" Ref="#PWR?"  Part="1" 
AR Path="/5F9CF0DB/60D44292" Ref="#PWR?"  Part="1" 
AR Path="/60088CAB/60D44292" Ref="#PWR?"  Part="1" 
AR Path="/60C45676/60D44292" Ref="#PWR039"  Part="1" 
F 0 "#PWR039" H 8400 5560 50  0001 C CNN
F 1 "GND" H 8405 5637 50  0000 C CNN
F 2 "" H 8400 5810 50  0001 C CNN
F 3 "" H 8400 5810 50  0001 C CNN
	1    8400 5810
	-1   0    0    1   
$EndComp
$Comp
L power:VCC #PWR041
U 1 1 60D44298
P 8550 5800
F 0 "#PWR041" H 8550 5650 50  0001 C CNN
F 1 "VCC" H 8567 5973 50  0000 C CNN
F 2 "" H 8550 5800 50  0001 C CNN
F 3 "" H 8550 5800 50  0001 C CNN
	1    8550 5800
	1    0    0    -1  
$EndComp
Wire Wire Line
	8550 5800 8550 5850
Wire Wire Line
	8550 5850 8590 5850
Wire Wire Line
	8400 5810 8400 5950
Wire Wire Line
	8400 5950 8590 5950
$Comp
L power:GND #PWR?
U 1 1 60D71B58
P 9480 5760
AR Path="/60D71B58" Ref="#PWR?"  Part="1" 
AR Path="/5F2CBBC0/60D71B58" Ref="#PWR?"  Part="1" 
AR Path="/5F9CF0DB/60D71B58" Ref="#PWR?"  Part="1" 
AR Path="/60088CAB/60D71B58" Ref="#PWR?"  Part="1" 
AR Path="/60C45676/60D71B58" Ref="#PWR066"  Part="1" 
F 0 "#PWR066" H 9480 5510 50  0001 C CNN
F 1 "GND" H 9485 5587 50  0000 C CNN
F 2 "" H 9480 5760 50  0001 C CNN
F 3 "" H 9480 5760 50  0001 C CNN
	1    9480 5760
	-1   0    0    1   
$EndComp
$Comp
L power:VCC #PWR067
U 1 1 60D71B5E
P 9630 5750
F 0 "#PWR067" H 9630 5600 50  0001 C CNN
F 1 "VCC" H 9647 5923 50  0000 C CNN
F 2 "" H 9630 5750 50  0001 C CNN
F 3 "" H 9630 5750 50  0001 C CNN
	1    9630 5750
	1    0    0    -1  
$EndComp
Wire Wire Line
	9630 5750 9630 5800
Wire Wire Line
	9630 5800 9670 5800
Wire Wire Line
	9480 5760 9480 5900
Wire Wire Line
	9480 5900 9670 5900
$Comp
L power:GND #PWR?
U 1 1 60DB962C
P 8920 4060
AR Path="/60DB962C" Ref="#PWR?"  Part="1" 
AR Path="/5F2CBBC0/60DB962C" Ref="#PWR?"  Part="1" 
AR Path="/5F9CF0DB/60DB962C" Ref="#PWR?"  Part="1" 
AR Path="/60088CAB/60DB962C" Ref="#PWR?"  Part="1" 
AR Path="/60C45676/60DB962C" Ref="#PWR044"  Part="1" 
F 0 "#PWR044" H 8920 3810 50  0001 C CNN
F 1 "GND" H 8925 3887 50  0000 C CNN
F 2 "" H 8920 4060 50  0001 C CNN
F 3 "" H 8920 4060 50  0001 C CNN
	1    8920 4060
	-1   0    0    1   
$EndComp
$Comp
L power:VCC #PWR056
U 1 1 60DB9632
P 9070 4050
F 0 "#PWR056" H 9070 3900 50  0001 C CNN
F 1 "VCC" H 9087 4223 50  0000 C CNN
F 2 "" H 9070 4050 50  0001 C CNN
F 3 "" H 9070 4050 50  0001 C CNN
	1    9070 4050
	1    0    0    -1  
$EndComp
Wire Wire Line
	9070 4050 9070 4100
Wire Wire Line
	9070 4100 9110 4100
Wire Wire Line
	8920 4060 8920 4200
Wire Wire Line
	8920 4200 9110 4200
$Comp
L Connector_Generic:Conn_01x04 debugSerie?
U 1 1 60DE8B07
P 9300 4800
AR Path="/60DE8B07" Ref="debugSerie?"  Part="1" 
AR Path="/60088CAB/60DE8B07" Ref="debugSerie?"  Part="1" 
AR Path="/60C45676/60DE8B07" Ref="debugSerie2"  Part="1" 
F 0 "debugSerie2" H 8400 4950 50  0000 L CNN
F 1 "Conn_1x04_P2,54mm" H 8100 4500 50  0001 L CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_1x04_P2.54mm_Vertical" H 9300 4800 50  0001 C CNN
F 3 "~" H 9300 4800 50  0001 C CNN
	1    9300 4800
	1    0    0    -1  
$EndComp
Wire Wire Line
	9080 4900 9100 4900
Wire Wire Line
	9080 5000 9100 5000
$Comp
L power:GND #PWR?
U 1 1 60DE8B13
P 8910 4660
AR Path="/60DE8B13" Ref="#PWR?"  Part="1" 
AR Path="/5F2CBBC0/60DE8B13" Ref="#PWR?"  Part="1" 
AR Path="/5F9CF0DB/60DE8B13" Ref="#PWR?"  Part="1" 
AR Path="/60088CAB/60DE8B13" Ref="#PWR?"  Part="1" 
AR Path="/60C45676/60DE8B13" Ref="#PWR042"  Part="1" 
F 0 "#PWR042" H 8910 4410 50  0001 C CNN
F 1 "GND" H 8915 4487 50  0000 C CNN
F 2 "" H 8910 4660 50  0001 C CNN
F 3 "" H 8910 4660 50  0001 C CNN
	1    8910 4660
	-1   0    0    1   
$EndComp
Wire Wire Line
	9060 4650 9060 4700
Wire Wire Line
	9060 4700 9100 4700
Wire Wire Line
	8910 4660 8910 4800
Wire Wire Line
	8910 4800 9100 4800
Text Notes 9520 5020 1    50   ~ 0
conn_DBG
Text GLabel 9080 4900 0    50   Input ~ 0
PA2|A2
Text GLabel 9080 5000 0    50   Input ~ 0
PA3|A3
$Comp
L power:+3.3V #PWR?
U 1 1 60E81B22
P 9060 4650
AR Path="/60E81B22" Ref="#PWR?"  Part="1" 
AR Path="/5F2CBBC0/60E81B22" Ref="#PWR?"  Part="1" 
AR Path="/5F9CF0DB/60E81B22" Ref="#PWR?"  Part="1" 
AR Path="/60088CAB/60E81B22" Ref="#PWR?"  Part="1" 
AR Path="/60C45676/60E81B22" Ref="#PWR049"  Part="1" 
F 0 "#PWR049" H 9060 4500 50  0001 C CNN
F 1 "+3.3V" H 9075 4823 50  0000 C CNN
F 2 "" H 9060 4650 50  0001 C CNN
F 3 "" H 9060 4650 50  0001 C CNN
	1    9060 4650
	1    0    0    -1  
$EndComp
$Comp
L Regulator_Linear:AMS1117-3.3 U1
U 1 1 60CF13FB
P 8500 900
F 0 "U1" H 8500 1142 50  0000 C CNN
F 1 "AMS1117-3.3" H 8500 1051 50  0000 C CNN
F 2 "Package_TO_SOT_SMD:SOT-223-3_TabPin2" H 8500 1100 50  0001 C CNN
F 3 "http://www.advanced-monolithic.com/pdf/ds1117.pdf" H 8600 650 50  0001 C CNN
	1    8500 900 
	1    0    0    -1  
$EndComp
Wire Wire Line
	7950 900  8200 900 
Wire Wire Line
	8800 900  9100 900 
Text GLabel 3750 1950 2    50   Input ~ 0
INT1_ADXL
Text GLabel 3750 2050 2    50   Input ~ 0
INT2_ADXL
Wire Wire Line
	3500 1950 3750 1950
Wire Wire Line
	3500 2050 3750 2050
$Comp
L power:GND #PWR?
U 1 1 60CFECCC
P 4450 4050
AR Path="/60CFECCC" Ref="#PWR?"  Part="1" 
AR Path="/5F2CBBC0/60CFECCC" Ref="#PWR?"  Part="1" 
AR Path="/5F9CF0DB/60CFECCC" Ref="#PWR?"  Part="1" 
AR Path="/60088CAB/60CFECCC" Ref="#PWR?"  Part="1" 
AR Path="/60C45676/60CFECCC" Ref="#PWR085"  Part="1" 
F 0 "#PWR085" H 4450 3800 50  0001 C CNN
F 1 "GND" H 4455 3877 50  0000 C CNN
F 2 "" H 4450 4050 50  0001 C CNN
F 3 "" H 4450 4050 50  0001 C CNN
	1    4450 4050
	-1   0    0    1   
$EndComp
Text GLabel 3750 2650 2    50   Input ~ 0
PB14|D12
Text GLabel 3750 2750 2    50   Input ~ 0
PB15|D11
$Comp
L RF_Module:CMWX1ZZABZ-078 Murata?
U 1 1 600AF0C9
P 2600 2750
AR Path="/600AF0C9" Ref="Murata?"  Part="1" 
AR Path="/60088CAB/600AF0C9" Ref="Murata1"  Part="1" 
AR Path="/60C45676/600AF0C9" Ref="Murata1"  Part="1" 
F 0 "Murata1" H 1850 4350 50  0000 C CNN
F 1 "CMWX1ZZABZ-078" H 1850 4250 50  0000 C CNN
F 2 "_uC:MURATA-LoRAWAM" H 2600 2750 50  0001 C CNN
F 3 "https://wireless.murata.com/RFM/data/type_abz.pdf" H 5050 1250 50  0001 C CNN
	1    2600 2750
	1    0    0    -1  
$EndComp
Wire Wire Line
	3500 2650 3750 2650
Wire Wire Line
	3500 2750 3750 2750
Text Label 3650 2450 0    50   ~ 0
CS
Text Label 3600 2550 0    50   ~ 0
SCK
Text Label 3550 2650 0    50   ~ 0
MISO
Text Label 3550 2750 0    50   ~ 0
MOSI
Text GLabel 4750 4550 0    50   Input ~ 0
PB13|D13
Text GLabel 4750 4650 0    50   Input ~ 0
PB12|D10
Text GLabel 4750 4450 0    50   Input ~ 0
PB14|D12
Text GLabel 4750 4350 0    50   Input ~ 0
PB15|D11
$Comp
L Connector_Generic:Conn_01x06 J14
U 1 1 60CE79F7
P 4950 4350
F 0 "J14" H 5030 4342 50  0000 L CNN
F 1 "SPI_Conn" H 5030 4251 50  0000 L CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_1x06_P2.54mm_Vertical" H 4950 4350 50  0001 C CNN
F 3 "~" H 4950 4350 50  0001 C CNN
	1    4950 4350
	1    0    0    -1  
$EndComp
$Comp
L power:+3.3V #PWR086
U 1 1 60DCD433
P 4650 4050
F 0 "#PWR086" H 4650 3900 50  0001 C CNN
F 1 "+3.3V" H 4665 4223 50  0000 C CNN
F 2 "" H 4650 4050 50  0001 C CNN
F 3 "" H 4650 4050 50  0001 C CNN
	1    4650 4050
	1    0    0    -1  
$EndComp
Wire Wire Line
	4650 4050 4650 4150
Wire Wire Line
	4650 4150 4750 4150
Wire Wire Line
	4450 4050 4450 4250
Wire Wire Line
	4450 4250 4750 4250
$EndSCHEMATC

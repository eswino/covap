EESchema Schematic File Version 4
EELAYER 30 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 9 9
Title "Tracker-LRWAN"
Date ""
Rev "v006"
Comp "UCO TIC 173"
Comment1 "Giraopa"
Comment2 "SENSORYCA SLU"
Comment3 "Francisco M Alvarez Wic"
Comment4 ""
$EndDescr
$Comp
L Sensor_Motion:ADXL343 U?
U 1 1 5ED96B7E
P 7500 1400
AR Path="/5ED96B7E" Ref="U?"  Part="1" 
AR Path="/5ED71ECF/5ED96B7E" Ref="U5"  Part="1" 
AR Path="/5FC14161/5ED96B7E" Ref="U1"  Part="1" 
AR Path="/60D3388F/5ED96B7E" Ref="U9"  Part="1" 
F 0 "U9" H 7750 1500 50  0000 L CNN
F 1 "ADXL345" H 7550 1200 50  0000 L CNN
F 2 "_Sensores:ADXL_345" H 7500 1400 50  0001 C CNN
F 3 "https://www.analog.com/media/en/technical-documentation/data-sheets/ADXL345.pdf" H 7500 1400 50  0001 C CNN
	1    7500 1400
	1    0    0    -1  
$EndComp
NoConn ~ 7300 900 
NoConn ~ 7700 1900
Wire Wire Line
	7500 850  7700 850 
Wire Wire Line
	7700 850  7700 900 
Wire Wire Line
	7500 850  7500 900 
Wire Wire Line
	7500 850  7500 750 
Connection ~ 7500 850 
$Comp
L power:+3.3V #PWR?
U 1 1 5ED96B8B
P 7500 750
AR Path="/5ED96B8B" Ref="#PWR?"  Part="1" 
AR Path="/5ED71ECF/5ED96B8B" Ref="#PWR0112"  Part="1" 
AR Path="/5FC14161/5ED96B8B" Ref="#PWR036"  Part="1" 
AR Path="/60D3388F/5ED96B8B" Ref="#PWR083"  Part="1" 
F 0 "#PWR083" H 7500 600 50  0001 C CNN
F 1 "+3.3V" H 7400 900 50  0000 C CNN
F 2 "" H 7500 750 50  0000 C CNN
F 3 "" H 7500 750 50  0000 C CNN
	1    7500 750 
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR?
U 1 1 5ED96B97
P 7500 2000
AR Path="/5ED96B97" Ref="#PWR?"  Part="1" 
AR Path="/5ED71ECF/5ED96B97" Ref="#PWR0113"  Part="1" 
AR Path="/5FC14161/5ED96B97" Ref="#PWR042"  Part="1" 
AR Path="/60D3388F/5ED96B97" Ref="#PWR084"  Part="1" 
F 0 "#PWR084" H 7500 1750 50  0001 C CNN
F 1 "GND" H 7505 1827 50  0000 C CNN
F 2 "" H 7500 2000 50  0001 C CNN
F 3 "" H 7500 2000 50  0001 C CNN
	1    7500 2000
	1    0    0    -1  
$EndComp
Wire Wire Line
	7500 2000 7500 1900
$Comp
L Device:R r?
U 1 1 5ED96B9E
P 3250 1000
AR Path="/5ED96B9E" Ref="r?"  Part="1" 
AR Path="/5ED71ECF/5ED96B9E" Ref="R_adxl_2"  Part="1" 
AR Path="/5FC14161/5ED96B9E" Ref="R11"  Part="1" 
AR Path="/60D3388F/5ED96B9E" Ref="R17"  Part="1" 
F 0 "R17" V 3350 750 50  0000 L CNN
F 1 "10k" V 3250 950 50  0000 L CNN
F 2 "Resistor_SMD:R_0603_1608Metric_Pad1.05x0.95mm_HandSolder" V 3180 1000 50  0001 C CNN
F 3 "~" H 3250 1000 50  0001 C CNN
	1    3250 1000
	0    -1   -1   0   
$EndComp
Wire Wire Line
	3100 1000 3050 1000
Wire Wire Line
	3050 1000 3050 900 
$Comp
L power:+3.3V #PWR?
U 1 1 5ED96BA8
P 3050 900
AR Path="/5ED96BA8" Ref="#PWR?"  Part="1" 
AR Path="/5ED71ECF/5ED96BA8" Ref="#PWR0114"  Part="1" 
AR Path="/5FC14161/5ED96BA8" Ref="#PWR026"  Part="1" 
AR Path="/60D3388F/5ED96BA8" Ref="#PWR081"  Part="1" 
F 0 "#PWR081" H 3050 750 50  0001 C CNN
F 1 "+3.3V" H 3100 700 50  0000 C CNN
F 2 "" H 3050 900 50  0000 C CNN
F 3 "" H 3050 900 50  0000 C CNN
	1    3050 900 
	1    0    0    -1  
$EndComp
Wire Wire Line
	6850 1300 7000 1300
Wire Wire Line
	6850 1600 7000 1600
Wire Wire Line
	7000 1400 6800 1400
Wire Wire Line
	7000 1500 6800 1500
Wire Wire Line
	8000 1400 8250 1400
Wire Wire Line
	8000 1500 8250 1500
Text GLabel 1200 1000 0    50   Input ~ 0
INT1_ADXL
Text GLabel 1200 900  0    50   Input ~ 0
INT2_ADXL
Wire Wire Line
	1400 700  1200 700 
Wire Wire Line
	1400 800  1200 800 
Text Label 1400 700  0    50   ~ 0
sda_adxl
Text Label 1400 800  0    50   ~ 0
scl_adxl
Text Label 6800 1500 2    50   ~ 0
scl_adxl
Text Label 6850 1600 2    50   ~ 0
cs_adxl
Text Label 6800 1400 2    50   ~ 0
sda_adxl
$Comp
L Device:R r?
U 1 1 5ED96B91
P 5250 900
AR Path="/5ED96B91" Ref="r?"  Part="1" 
AR Path="/5ED71ECF/5ED96B91" Ref="R_adxl_1"  Part="1" 
AR Path="/5FC14161/5ED96B91" Ref="R12"  Part="1" 
AR Path="/60D3388F/5ED96B91" Ref="R18"  Part="1" 
F 0 "R18" V 5150 750 50  0000 L CNN
F 1 "10k" V 5250 850 50  0000 L CNN
F 2 "Resistor_SMD:R_0603_1608Metric_Pad1.05x0.95mm_HandSolder" V 5180 900 50  0001 C CNN
F 3 "~" H 5250 900 50  0001 C CNN
	1    5250 900 
	0    -1   1    0   
$EndComp
Text Label 5500 900  0    50   ~ 0
addr_adxl
Wire Wire Line
	5500 900  5400 900 
$Comp
L power:GND #PWR?
U 1 1 5ED9A953
P 5000 950
AR Path="/5ED9A953" Ref="#PWR?"  Part="1" 
AR Path="/5ED71ECF/5ED9A953" Ref="#PWR0115"  Part="1" 
AR Path="/5FC14161/5ED9A953" Ref="#PWR027"  Part="1" 
AR Path="/60D3388F/5ED9A953" Ref="#PWR082"  Part="1" 
F 0 "#PWR082" H 5000 700 50  0001 C CNN
F 1 "GND" H 5005 777 50  0000 C CNN
F 2 "" H 5000 950 50  0001 C CNN
F 3 "" H 5000 950 50  0001 C CNN
	1    5000 950 
	-1   0    0    -1  
$EndComp
Wire Wire Line
	5100 900  5000 900 
Wire Wire Line
	5000 900  5000 950 
Wire Wire Line
	3400 1000 3500 1000
Text Label 3500 1000 0    50   ~ 0
cs_adxl
Text Label 6850 1300 2    50   ~ 0
addr_adxl
Wire Wire Line
	1200 900  1400 900 
Wire Wire Line
	1200 1000 1400 1000
Text Label 8250 1400 0    50   ~ 0
int1
Text Label 8250 1500 0    50   ~ 0
int2
Text Label 1400 900  0    50   ~ 0
int1
Text Label 1400 1000 0    50   ~ 0
int2
Wire Notes Line
	8500 2350 500  2350
Wire Notes Line
	2350 500  2350 2600
Wire Notes Line
	4300 2600 4300 550 
Wire Notes Line
	500  2600 4300 2600
Wire Notes Line
	6300 2600 6300 650 
Wire Notes Line
	8500 500  8500 2600
Wire Notes Line
	4350 2600 8500 2600
Text Notes 850  2500 0    50   ~ 0
Salida etiquetas globales
Text Notes 2900 2500 0    50   ~ 0
Activar acelerómetro
Text Notes 4800 2500 0    50   ~ 0
Dirección I2C acelerómetro
Text Notes 7200 2500 0    50   ~ 0
Acelerómetro
Text Notes 950  2000 0    50   ~ 0
ADDR [BUS]: 0x53\nADDR [WRITE]: 0xA6\nADDR [READ]: 0xA7
Text GLabel 1200 800  0    50   Input ~ 0
PB8|SCL
Text GLabel 1200 700  0    50   Input ~ 0
PB9|SDA
$EndSCHEMATC

EESchema Schematic File Version 4
EELAYER 30 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 7 9
Title "Tracker-LRWAN"
Date ""
Rev "v006"
Comp "UCO TIC 173"
Comment1 "Giraopa"
Comment2 "SENSORYCA SLU"
Comment3 "Francisco M Alvarez Wic"
Comment4 ""
$EndDescr
$Comp
L _CI:L86_Quectel U?
U 1 1 5E56E55F
P 9450 3450
AR Path="/5E56E55F" Ref="U?"  Part="1" 
AR Path="/5E55C83F/5E56E55F" Ref="U2"  Part="1" 
AR Path="/5FC14EA4/5E56E55F" Ref="U2"  Part="1" 
AR Path="/60C864F6/5E56E55F" Ref="U5"  Part="1" 
F 0 "U5" H 9375 3965 50  0000 C CNN
F 1 "L86_Quectel" H 9375 3874 50  0000 C CNN
F 2 "_Sensores:QUELTEC_L86" H 9450 3450 50  0001 C CNN
F 3 "" H 9450 3450 50  0001 C CNN
	1    9450 3450
	1    0    0    -1  
$EndComp
Wire Wire Line
	8650 3550 9000 3550
Wire Wire Line
	9000 3650 8650 3650
Wire Wire Line
	10300 3350 9950 3350
Wire Wire Line
	9950 3650 10300 3650
Wire Wire Line
	10300 3750 9950 3750
$Comp
L power:GND #PWR?
U 1 1 5E56E577
P 8360 4060
AR Path="/5E56E577" Ref="#PWR?"  Part="1" 
AR Path="/5E55C83F/5E56E577" Ref="#PWR0140"  Part="1" 
AR Path="/5FC14EA4/5E56E577" Ref="#PWR048"  Part="1" 
AR Path="/60C864F6/5E56E577" Ref="#PWR073"  Part="1" 
F 0 "#PWR073" H 8360 3810 50  0001 C CNN
F 1 "GND" H 8365 3887 50  0000 C CNN
F 2 "" H 8360 4060 50  0001 C CNN
F 3 "" H 8360 4060 50  0001 C CNN
	1    8360 4060
	1    0    0    -1  
$EndComp
Wire Wire Line
	8800 3750 9000 3750
NoConn ~ 9000 3450
Wire Wire Line
	1940 1020 1940 920 
$Comp
L Device:C C?
U 1 1 5E56E584
P 1940 1320
AR Path="/5E56E584" Ref="C?"  Part="1" 
AR Path="/5E55C83F/5E56E584" Ref="C_GPS_1"  Part="1" 
AR Path="/5FC14EA4/5E56E584" Ref="C13"  Part="1" 
AR Path="/60C864F6/5E56E584" Ref="C19"  Part="1" 
F 0 "C19" H 2055 1366 50  0000 L CNN
F 1 "100nF" H 2055 1275 50  0000 L CNN
F 2 "Capacitor_SMD:C_0603_1608Metric_Pad1.05x0.95mm_HandSolder" H 1978 1170 50  0001 C CNN
F 3 "https://datasheet.lcsc.com/szlcsc/YAGEO-AC0603KRX7R9BB104_C149620.pdf" H 1940 1320 50  0001 C CNN
	1    1940 1320
	1    0    0    -1  
$EndComp
$Comp
L Device:C C?
U 1 1 5E56E58A
P 2290 1320
AR Path="/5E56E58A" Ref="C?"  Part="1" 
AR Path="/5E55C83F/5E56E58A" Ref="C_GPS_2"  Part="1" 
AR Path="/5FC14EA4/5E56E58A" Ref="C14"  Part="1" 
AR Path="/60C864F6/5E56E58A" Ref="C20"  Part="1" 
F 0 "C20" H 2405 1366 50  0000 L CNN
F 1 "10uF" H 2405 1275 50  0000 L CNN
F 2 "Capacitor_SMD:C_0805_2012Metric_Pad1.15x1.40mm_HandSolder" H 2328 1170 50  0001 C CNN
F 3 "https://datasheet.lcsc.com/szlcsc/YAGEO-AC0603KRX7R9BB104_C149620.pdf" H 2290 1320 50  0001 C CNN
	1    2290 1320
	1    0    0    -1  
$EndComp
Wire Wire Line
	2290 1020 2290 1170
Wire Wire Line
	2290 1620 2290 1470
Wire Wire Line
	1940 1470 1940 1620
Wire Wire Line
	1940 1620 2290 1620
Wire Wire Line
	1940 1170 1940 1020
Connection ~ 1940 1020
Wire Wire Line
	1940 1020 2290 1020
$Comp
L power:+3.3V #PWR?
U 1 1 5E56E5A0
P 1940 920
AR Path="/5E56E5A0" Ref="#PWR?"  Part="1" 
AR Path="/5E55C83F/5E56E5A0" Ref="#PWR0141"  Part="1" 
AR Path="/5FC14EA4/5E56E5A0" Ref="#PWR043"  Part="1" 
AR Path="/60C864F6/5E56E5A0" Ref="#PWR068"  Part="1" 
F 0 "#PWR068" H 1940 770 50  0001 C CNN
F 1 "+3.3V" H 2090 920 50  0000 C CNN
F 2 "" H 1940 920 50  0000 C CNN
F 3 "" H 1940 920 50  0000 C CNN
	1    1940 920 
	1    0    0    -1  
$EndComp
NoConn ~ 9950 3250
$Comp
L Jumper:Jumper_2_Open JP?
U 1 1 5E57D935
P 7900 1050
AR Path="/5E57D935" Ref="JP?"  Part="1" 
AR Path="/5E55C83F/5E57D935" Ref="JP_GPS_ANT1"  Part="1" 
AR Path="/5FC14EA4/5E57D935" Ref="JP1"  Part="1" 
AR Path="/60C864F6/5E57D935" Ref="JP1"  Part="1" 
F 0 "JP1" H 7900 1200 50  0000 C CNN
F 1 "JP_2P_Open" V 7900 700 50  0000 C CNN
F 2 "Jumper:SolderJumper-2_P1.3mm_Open_RoundedPad1.0x1.5mm" H 7900 1050 50  0001 C CNN
F 3 "~" H 7900 1050 50  0001 C CNN
	1    7900 1050
	1    0    0    -1  
$EndComp
$Comp
L Device:R R_?
U 1 1 5E57D93C
P 8500 1050
AR Path="/5E57D93C" Ref="R_?"  Part="1" 
AR Path="/5E55C83F/5E57D93C" Ref="R_GPS_filt-ant_1"  Part="1" 
AR Path="/5FC14EA4/5E57D93C" Ref="R10"  Part="1" 
AR Path="/60C864F6/5E57D93C" Ref="R_1"  Part="1" 
F 0 "R_1" V 8400 1050 50  0000 C CNN
F 1 "0" V 8500 1050 50  0000 C CNN
F 2 "Resistor_SMD:R_0603_1608Metric_Pad1.05x0.95mm_HandSolder" V 8430 1050 50  0001 C CNN
F 3 "https://datasheet.lcsc.com/szlcsc/YAGEO-RC0603FR-0710KL_C98220.pdf" H 8500 1050 50  0001 C CNN
	1    8500 1050
	0    1    1    0   
$EndComp
$Comp
L Device:C C?
U 1 1 5E57D942
P 8700 1350
AR Path="/5E57D942" Ref="C?"  Part="1" 
AR Path="/5E55C83F/5E57D942" Ref="C_GPS_filt-ant_2"  Part="1" 
AR Path="/5FC14EA4/5E57D942" Ref="C18"  Part="1" 
AR Path="/60C864F6/5E57D942" Ref="C24"  Part="1" 
F 0 "C24" V 8900 950 50  0000 L CNN
F 1 "NC" H 8550 1300 50  0000 L CNN
F 2 "Capacitor_SMD:C_0603_1608Metric_Pad1.05x0.95mm_HandSolder" H 8738 1200 50  0001 C CNN
F 3 "https://datasheet.lcsc.com/szlcsc/YAGEO-AC0603KRX7R9BB104_C149620.pdf" H 8700 1350 50  0001 C CNN
	1    8700 1350
	1    0    0    -1  
$EndComp
$Comp
L Device:C C?
U 1 1 5E57D948
P 8250 1350
AR Path="/5E57D948" Ref="C?"  Part="1" 
AR Path="/5E55C83F/5E57D948" Ref="C_GPS_filt-ant_1"  Part="1" 
AR Path="/5FC14EA4/5E57D948" Ref="C17"  Part="1" 
AR Path="/60C864F6/5E57D948" Ref="C23"  Part="1" 
F 0 "C23" V 8100 900 50  0000 L CNN
F 1 "NC" H 8365 1305 50  0000 L CNN
F 2 "Capacitor_SMD:C_0603_1608Metric_Pad1.05x0.95mm_HandSolder" H 8288 1200 50  0001 C CNN
F 3 "https://datasheet.lcsc.com/szlcsc/YAGEO-AC0603KRX7R9BB104_C149620.pdf" H 8250 1350 50  0001 C CNN
	1    8250 1350
	1    0    0    -1  
$EndComp
Wire Wire Line
	8100 1050 8250 1050
Wire Wire Line
	8250 1050 8250 1200
Wire Wire Line
	8650 1050 8700 1050
Wire Wire Line
	8700 1200 8700 1050
Connection ~ 8700 1050
Wire Wire Line
	8700 1050 8950 1050
Wire Wire Line
	8250 1500 8250 1700
Wire Wire Line
	8700 1700 8700 1500
$Comp
L power:GND #PWR?
U 1 1 5E57D957
P 8550 1800
AR Path="/5E57D957" Ref="#PWR?"  Part="1" 
AR Path="/5E55C83F/5E57D957" Ref="#PWR0142"  Part="1" 
AR Path="/5FC14EA4/5E57D957" Ref="#PWR049"  Part="1" 
AR Path="/60C864F6/5E57D957" Ref="#PWR074"  Part="1" 
F 0 "#PWR074" H 8550 1550 50  0001 C CNN
F 1 "GND" H 8555 1627 50  0000 C CNN
F 2 "" H 8550 1800 50  0001 C CNN
F 3 "" H 8550 1800 50  0001 C CNN
	1    8550 1800
	1    0    0    -1  
$EndComp
Wire Wire Line
	8550 1700 8550 1800
Connection ~ 8550 1700
Wire Wire Line
	8550 1700 8700 1700
Connection ~ 8250 1050
Wire Wire Line
	8250 1050 8350 1050
Wire Wire Line
	8250 1700 8550 1700
$Comp
L Connector:Conn_Coaxial J?
U 1 1 5E585702
P 9950 1050
AR Path="/5E585702" Ref="J?"  Part="1" 
AR Path="/5E55C83F/5E585702" Ref="J4"  Part="1" 
AR Path="/5FC14EA4/5E585702" Ref="J8"  Part="1" 
AR Path="/60C864F6/5E585702" Ref="J18"  Part="1" 
F 0 "J18" H 10050 1025 50  0000 L CNN
F 1 "GPS_ANT" H 10050 934 50  0000 L CNN
F 2 "Connector_Coaxial:U.FL_Molex_MCRF_73412-0110_Vertical" H 9950 1050 50  0001 C CNN
F 3 "https://www.mouser.es/datasheet/2/18/CA-1JB-713210.pdf" H 9950 1050 50  0001 C CNN
	1    9950 1050
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR?
U 1 1 5E585709
P 9950 1350
AR Path="/5E585709" Ref="#PWR?"  Part="1" 
AR Path="/5E55C83F/5E585709" Ref="#PWR0143"  Part="1" 
AR Path="/5FC14EA4/5E585709" Ref="#PWR050"  Part="1" 
AR Path="/60C864F6/5E585709" Ref="#PWR075"  Part="1" 
F 0 "#PWR075" H 9950 1100 50  0001 C CNN
F 1 "GND" H 9955 1177 50  0000 C CNN
F 2 "" H 9950 1350 50  0001 C CNN
F 3 "" H 9950 1350 50  0001 C CNN
	1    9950 1350
	1    0    0    -1  
$EndComp
Wire Wire Line
	9950 1250 9950 1350
$Comp
L Device:R R?
U 1 1 5E5A45A7
P 3670 1000
AR Path="/5E5A45A7" Ref="R?"  Part="1" 
AR Path="/5E55C83F/5E5A45A7" Ref="R_GPS_rst1"  Part="1" 
AR Path="/5FC14EA4/5E5A45A7" Ref="R5"  Part="1" 
AR Path="/60C864F6/5E5A45A7" Ref="R24"  Part="1" 
F 0 "R24" V 3570 1000 50  0000 C CNN
F 1 "10k" V 3670 1000 50  0000 C CNN
F 2 "Resistor_SMD:R_0603_1608Metric_Pad1.05x0.95mm_HandSolder" V 3600 1000 50  0001 C CNN
F 3 "https://datasheet.lcsc.com/szlcsc/YAGEO-RC0603FR-0710KL_C98220.pdf" H 3670 1000 50  0001 C CNN
	1    3670 1000
	-1   0    0    1   
$EndComp
$Comp
L power:GND #PWR?
U 1 1 5E5A45B2
P 3670 1770
AR Path="/5E5A45B2" Ref="#PWR?"  Part="1" 
AR Path="/5E55C83F/5E5A45B2" Ref="#PWR0144"  Part="1" 
AR Path="/5FC14EA4/5E5A45B2" Ref="#PWR045"  Part="1" 
AR Path="/60C864F6/5E5A45B2" Ref="#PWR070"  Part="1" 
F 0 "#PWR070" H 3670 1520 50  0001 C CNN
F 1 "GND" H 3675 1597 50  0000 C CNN
F 2 "" H 3670 1770 50  0001 C CNN
F 3 "" H 3670 1770 50  0001 C CNN
	1    3670 1770
	1    0    0    -1  
$EndComp
Text Label 8650 3550 0    50   ~ 0
reset
Text Label 3380 1210 0    50   ~ 0
reset
Text Label 8650 3650 0    50   ~ 0
antenna
Wire Wire Line
	9750 1050 9500 1050
Wire Wire Line
	7700 1050 7550 1050
Text Label 7550 1050 2    50   ~ 0
antenna
Text Label 8950 1050 0    50   ~ 0
ant_gps
Text Label 9500 1050 0    50   ~ 0
ant_gps
Text Label 10300 3350 2    50   ~ 0
v_bckp
$Comp
L power:+3.3V #PWR?
U 1 1 5E62AA28
P 10450 3350
AR Path="/5E62AA28" Ref="#PWR?"  Part="1" 
AR Path="/5E55C83F/5E62AA28" Ref="#PWR0145"  Part="1" 
AR Path="/5FC14EA4/5E62AA28" Ref="#PWR051"  Part="1" 
AR Path="/60C864F6/5E62AA28" Ref="#PWR076"  Part="1" 
F 0 "#PWR076" H 10450 3200 50  0001 C CNN
F 1 "+3.3V" V 10450 3600 50  0000 C CNN
F 2 "" H 10450 3350 50  0000 C CNN
F 3 "" H 10450 3350 50  0000 C CNN
	1    10450 3350
	1    0    0    -1  
$EndComp
Text Label 10300 3650 2    50   ~ 0
rx_gps
Text Label 10300 3750 2    50   ~ 0
tx_gps
Text Label 1210 940  0    50   ~ 0
rx_gps
Text Label 1210 1040 0    50   ~ 0
tx_gps
$Comp
L power:GND #PWR?
U 1 1 5E632739
P 1940 1720
AR Path="/5E632739" Ref="#PWR?"  Part="1" 
AR Path="/5E55C83F/5E632739" Ref="#PWR0146"  Part="1" 
AR Path="/5FC14EA4/5E632739" Ref="#PWR044"  Part="1" 
AR Path="/60C864F6/5E632739" Ref="#PWR069"  Part="1" 
F 0 "#PWR069" H 1940 1470 50  0001 C CNN
F 1 "GND" H 1945 1547 50  0000 C CNN
F 2 "" H 1940 1720 50  0001 C CNN
F 3 "" H 1940 1720 50  0001 C CNN
	1    1940 1720
	1    0    0    -1  
$EndComp
Wire Wire Line
	1940 1620 1940 1720
Wire Wire Line
	9950 3450 10450 3450
Wire Wire Line
	10450 3450 10450 3350
Wire Wire Line
	9950 3550 10450 3550
NoConn ~ 9000 3250
NoConn ~ 9000 3350
Wire Wire Line
	5950 1650 5950 1750
$Comp
L power:GND #PWR?
U 1 1 5E622DE0
P 5950 1750
AR Path="/5E622DE0" Ref="#PWR?"  Part="1" 
AR Path="/5E55C83F/5E622DE0" Ref="#PWR0147"  Part="1" 
AR Path="/5FC14EA4/5E622DE0" Ref="#PWR047"  Part="1" 
AR Path="/60C864F6/5E622DE0" Ref="#PWR072"  Part="1" 
F 0 "#PWR072" H 5950 1500 50  0001 C CNN
F 1 "GND" H 5955 1577 50  0000 C CNN
F 2 "" H 5950 1750 50  0001 C CNN
F 3 "" H 5950 1750 50  0001 C CNN
	1    5950 1750
	1    0    0    -1  
$EndComp
Wire Wire Line
	4650 1050 4700 1050
Connection ~ 5600 1050
Wire Wire Line
	5500 1050 5600 1050
Text Label 6400 1050 0    50   ~ 0
v_bckp
Connection ~ 6250 1050
Wire Wire Line
	6250 1050 6400 1050
Wire Wire Line
	6250 1050 6250 1200
Wire Wire Line
	5000 1050 5200 1050
Wire Wire Line
	4650 950  4650 1050
Wire Wire Line
	5600 1500 5600 1650
$Comp
L Device:C C?
U 1 1 5E598BBF
P 5600 1350
AR Path="/5E598BBF" Ref="C?"  Part="1" 
AR Path="/5E55C83F/5E598BBF" Ref="C_GPS_vbck_1"  Part="1" 
AR Path="/5FC14EA4/5E598BBF" Ref="C15"  Part="1" 
AR Path="/60C864F6/5E598BBF" Ref="C21"  Part="1" 
F 0 "C21" V 5700 650 50  0000 L CNN
F 1 "100nF" H 5650 1250 50  0000 L CNN
F 2 "Capacitor_SMD:C_0603_1608Metric_Pad1.05x0.95mm_HandSolder" H 5638 1200 50  0001 C CNN
F 3 "https://datasheet.lcsc.com/szlcsc/YAGEO-AC0603KRX7R9BB104_C149620.pdf" H 5600 1350 50  0001 C CNN
	1    5600 1350
	1    0    0    -1  
$EndComp
$Comp
L Device:C C?
U 1 1 5E598BB9
P 5950 1350
AR Path="/5E598BB9" Ref="C?"  Part="1" 
AR Path="/5E55C83F/5E598BB9" Ref="C_GPS_vbck_2"  Part="1" 
AR Path="/5FC14EA4/5E598BB9" Ref="C16"  Part="1" 
AR Path="/60C864F6/5E598BB9" Ref="C22"  Part="1" 
F 0 "C22" V 6100 650 50  0000 L CNN
F 1 "4,7uF" H 6000 1250 50  0000 L CNN
F 2 "Capacitor_SMD:C_0805_2012Metric_Pad1.15x1.40mm_HandSolder" H 5988 1200 50  0001 C CNN
F 3 "https://datasheet.lcsc.com/szlcsc/YAGEO-AC0603KRX7R9BB104_C149620.pdf" H 5950 1350 50  0001 C CNN
	1    5950 1350
	1    0    0    -1  
$EndComp
$Comp
L Device:Battery_Cell BT?
U 1 1 5E598BB3
P 6250 1400
AR Path="/5E598BB3" Ref="BT?"  Part="1" 
AR Path="/5E55C83F/5E598BB3" Ref="Bat_GPS_Vbck1"  Part="1" 
AR Path="/5FC14EA4/5E598BB3" Ref="BT2"  Part="1" 
AR Path="/60C864F6/5E598BB3" Ref="BT2"  Part="1" 
F 0 "BT2" H 6368 1496 50  0000 L CNN
F 1 "Battery_Cell" H 6368 1405 50  0000 L CNN
F 2 "Battery:BatteryHolder_Seiko_MS621F" V 6250 1460 50  0001 C CNN
F 3 "https://datasheet.lcsc.com/szlcsc/1811061923_Q-J-CR2032-BS-6-1_C70377.pdf" V 6250 1460 50  0001 C CNN
	1    6250 1400
	1    0    0    -1  
$EndComp
Wire Wire Line
	5600 1650 5950 1650
Wire Wire Line
	6250 1650 6250 1500
Wire Wire Line
	5950 1500 5950 1650
Connection ~ 5950 1650
Wire Wire Line
	5950 1650 6250 1650
$Comp
L power:+3.3V #PWR?
U 1 1 5E598BA8
P 4650 950
AR Path="/5E598BA8" Ref="#PWR?"  Part="1" 
AR Path="/5E55C83F/5E598BA8" Ref="#PWR0148"  Part="1" 
AR Path="/5FC14EA4/5E598BA8" Ref="#PWR046"  Part="1" 
AR Path="/60C864F6/5E598BA8" Ref="#PWR071"  Part="1" 
F 0 "#PWR071" H 4650 800 50  0001 C CNN
F 1 "+3.3V" H 4650 1100 50  0000 C CNN
F 2 "" H 4650 950 50  0000 C CNN
F 3 "" H 4650 950 50  0000 C CNN
	1    4650 950 
	1    0    0    -1  
$EndComp
Wire Wire Line
	5950 1050 6250 1050
Connection ~ 5950 1050
Wire Wire Line
	5950 1200 5950 1050
Wire Wire Line
	5600 1050 5950 1050
Wire Wire Line
	5600 1200 5600 1050
$Comp
L Device:R R?
U 1 1 5E598B9C
P 5350 1050
AR Path="/5E598B9C" Ref="R?"  Part="1" 
AR Path="/5E55C83F/5E598B9C" Ref="R_GPS_vbck1"  Part="1" 
AR Path="/5FC14EA4/5E598B9C" Ref="R7"  Part="1" 
AR Path="/60C864F6/5E598B9C" Ref="R25"  Part="1" 
F 0 "R25" V 5143 1050 50  0000 C CNN
F 1 "1k" V 5234 1050 50  0000 C CNN
F 2 "Resistor_SMD:R_0603_1608Metric_Pad1.05x0.95mm_HandSolder" V 5280 1050 50  0001 C CNN
F 3 "https://datasheet.lcsc.com/szlcsc/YAGEO-RC0603FR-0710KL_C98220.pdf" H 5350 1050 50  0001 C CNN
	1    5350 1050
	0    -1   -1   0   
$EndComp
$Comp
L Device:D D?
U 1 1 5E598B96
P 4850 1050
AR Path="/5E598B96" Ref="D?"  Part="1" 
AR Path="/5E55C83F/5E598B96" Ref="D_vbck1"  Part="1" 
AR Path="/5FC14EA4/5E598B96" Ref="D3"  Part="1" 
AR Path="/60C864F6/5E598B96" Ref="D15"  Part="1" 
F 0 "D15" H 4900 1200 50  0000 R CNN
F 1 "schottky" V 4805 971 50  0000 R CNN
F 2 "Diode_SMD:D_SOD-123" H 4850 1050 50  0001 C CNN
F 3 "~" H 4850 1050 50  0001 C CNN
	1    4850 1050
	-1   0    0    1   
$EndComp
Connection ~ 1940 1620
Wire Notes Line
	500  2350 11100 2350
Wire Notes Line
	500  2350 500  2700
Wire Notes Line
	9350 500  9350 2700
Wire Notes Line
	7000 500  7000 2700
Wire Notes Line
	500  2700 11100 2700
Wire Notes Line
	4350 550  4350 2700
Wire Notes Line
	2700 500  2700 2700
Wire Notes Line
	1550 500  1550 2700
Text Notes 650  2550 0    50   ~ 0
Salida a etiquetas\nglobales\n
Text Notes 1600 2550 0    50   ~ 0
Alimentación estabilizada\n
Text Notes 2800 2550 0    50   ~ 0
Reset GPS
Text Notes 4400 2550 0    50   ~ 0
Alimentación gps backup\n
Text Notes 7100 2550 0    50   ~ 0
Salida gps antena
Text Notes 9400 2550 0    50   ~ 0
Conector GPS antena\n
Wire Notes Line
	11100 4400 8000 4400
Wire Notes Line
	8000 4700 11100 4700
Wire Notes Line
	8000 2700 8000 4700
Wire Notes Line
	11100 500  11100 4700
Text Notes 9050 4600 0    50   ~ 0
GPS Quectel L86
Text GLabel 1010 940  0    50   Input ~ 0
PA2|A2
Text GLabel 1010 1040 0    50   Input ~ 0
PA3|A3
$Comp
L power:+3.3V #PWR?
U 1 1 60E03ADA
P 3670 850
AR Path="/60E03ADA" Ref="#PWR?"  Part="1" 
AR Path="/5E55C83F/60E03ADA" Ref="#PWR?"  Part="1" 
AR Path="/5FC14EA4/60E03ADA" Ref="#PWR?"  Part="1" 
AR Path="/60C864F6/60E03ADA" Ref="#PWR078"  Part="1" 
F 0 "#PWR078" H 3670 700 50  0001 C CNN
F 1 "+3.3V" H 3670 1000 50  0000 C CNN
F 2 "" H 3670 850 50  0000 C CNN
F 3 "" H 3670 850 50  0000 C CNN
	1    3670 850 
	1    0    0    -1  
$EndComp
$Comp
L Switch:SW_Push swReset?
U 1 1 60E0B534
P 3370 1450
AR Path="/60E0B534" Ref="swReset?"  Part="1" 
AR Path="/60088CAB/60E0B534" Ref="swReset?"  Part="1" 
AR Path="/60C45676/60E0B534" Ref="swReset?"  Part="1" 
AR Path="/60C864F6/60E0B534" Ref="GPS_RST1"  Part="1" 
F 0 "GPS_RST1" V 3620 1800 50  0000 R CNN
F 1 "GPS_RST" V 3720 1800 50  0000 R CNN
F 2 "Button_Switch_SMD:SW_SPST_CK_RS282G05A3" H 3370 1650 50  0001 C CNN
F 3 "~" H 3370 1650 50  0001 C CNN
	1    3370 1450
	0    -1   -1   0   
$EndComp
$Comp
L Device:C_Small C?
U 1 1 60E0B53A
P 3670 1440
AR Path="/60E0B53A" Ref="C?"  Part="1" 
AR Path="/60088CAB/60E0B53A" Ref="C?"  Part="1" 
AR Path="/60C45676/60E0B53A" Ref="C?"  Part="1" 
AR Path="/60C864F6/60E0B53A" Ref="C25"  Part="1" 
F 0 "C25" H 3762 1486 50  0000 L CNN
F 1 "100nF" H 3762 1395 50  0000 L CNN
F 2 "Capacitor_SMD:C_0603_1608Metric_Pad1.05x0.95mm_HandSolder" H 3670 1440 50  0001 C CNN
F 3 "~" H 3670 1440 50  0001 C CNN
	1    3670 1440
	1    0    0    -1  
$EndComp
Wire Wire Line
	3370 1210 3670 1210
Wire Wire Line
	3670 1210 3670 1150
Wire Wire Line
	3370 1210 3370 1250
Wire Wire Line
	3670 1210 3670 1340
Connection ~ 3670 1210
Wire Wire Line
	3370 1650 3370 1700
Wire Wire Line
	3370 1700 3670 1700
Wire Wire Line
	3670 1700 3670 1540
Wire Wire Line
	3670 1700 3670 1770
Connection ~ 3670 1700
$Comp
L Device:Jumper_NC_Small JP2
U 1 1 60E3A55B
P 8540 3990
F 0 "JP2" H 8540 4202 50  0000 C CNN
F 1 "Jumper_NC_Small" H 8540 4111 50  0000 C CNN
F 2 "Jumper:SolderJumper-2_P1.3mm_Bridged2Bar_RoundedPad1.0x1.5mm" H 8540 3990 50  0001 C CNN
F 3 "~" H 8540 3990 50  0001 C CNN
	1    8540 3990
	1    0    0    -1  
$EndComp
Wire Wire Line
	8360 4060 8360 3990
Wire Wire Line
	8360 3990 8440 3990
Wire Wire Line
	8640 3990 8800 3990
Wire Wire Line
	8800 3750 8800 3990
Wire Wire Line
	10450 3550 10450 3990
$Comp
L power:GND #PWR?
U 1 1 60E43A93
P 10780 4060
AR Path="/60E43A93" Ref="#PWR?"  Part="1" 
AR Path="/5E55C83F/60E43A93" Ref="#PWR?"  Part="1" 
AR Path="/5FC14EA4/60E43A93" Ref="#PWR?"  Part="1" 
AR Path="/60C864F6/60E43A93" Ref="#PWR0101"  Part="1" 
F 0 "#PWR0101" H 10780 3810 50  0001 C CNN
F 1 "GND" H 10785 3887 50  0000 C CNN
F 2 "" H 10780 4060 50  0001 C CNN
F 3 "" H 10780 4060 50  0001 C CNN
	1    10780 4060
	-1   0    0    -1  
$EndComp
$Comp
L Device:Jumper_NC_Small JP3
U 1 1 60E43A99
P 10600 3990
F 0 "JP3" H 10600 4202 50  0000 C CNN
F 1 "Jumper_NC_Small" H 10600 4111 50  0000 C CNN
F 2 "Jumper:SolderJumper-2_P1.3mm_Bridged2Bar_RoundedPad1.0x1.5mm" H 10600 3990 50  0001 C CNN
F 3 "~" H 10600 3990 50  0001 C CNN
	1    10600 3990
	-1   0    0    -1  
$EndComp
Wire Wire Line
	10780 4060 10780 3990
Wire Wire Line
	10780 3990 10700 3990
Wire Wire Line
	10450 3990 10500 3990
$Comp
L Device:R_Small R?
U 1 1 60E613D2
P 1110 940
AR Path="/60E613D2" Ref="R?"  Part="1" 
AR Path="/5F2CBBC0/60E613D2" Ref="R?"  Part="1" 
AR Path="/5F9CF0DB/60E613D2" Ref="R?"  Part="1" 
AR Path="/60088CAB/60E613D2" Ref="R?"  Part="1" 
AR Path="/60C45676/60E613D2" Ref="R?"  Part="1" 
AR Path="/60C864F6/60E613D2" Ref="R22"  Part="1" 
F 0 "R22" V 1060 940 50  0000 C CNN
F 1 "0" V 1110 940 50  0000 C CNN
F 2 "Resistor_SMD:R_0603_1608Metric_Pad1.05x0.95mm_HandSolder" H 1110 940 50  0001 C CNN
F 3 "~" H 1110 940 50  0001 C CNN
	1    1110 940 
	0    1    1    0   
$EndComp
$Comp
L Device:R_Small R?
U 1 1 60E613D8
P 1110 1040
AR Path="/60E613D8" Ref="R?"  Part="1" 
AR Path="/5F2CBBC0/60E613D8" Ref="R?"  Part="1" 
AR Path="/5F9CF0DB/60E613D8" Ref="R?"  Part="1" 
AR Path="/60088CAB/60E613D8" Ref="R?"  Part="1" 
AR Path="/60C45676/60E613D8" Ref="R?"  Part="1" 
AR Path="/60C864F6/60E613D8" Ref="R23"  Part="1" 
F 0 "R23" V 1060 1040 50  0000 C CNN
F 1 "0" V 1110 1040 50  0000 C CNN
F 2 "Resistor_SMD:R_0603_1608Metric_Pad1.05x0.95mm_HandSolder" H 1110 1040 50  0001 C CNN
F 3 "~" H 1110 1040 50  0001 C CNN
	1    1110 1040
	0    1    1    0   
$EndComp
$EndSCHEMATC

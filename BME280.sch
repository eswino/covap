EESchema Schematic File Version 4
EELAYER 30 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 8 9
Title "Tracker-LRWAN"
Date ""
Rev "v006"
Comp "UCO TIC 173"
Comment1 "Giraopa"
Comment2 "SENSORYCA SLU"
Comment3 "Francisco M Alvarez Wic"
Comment4 ""
$EndDescr
$Comp
L Sensor:BME280 U8
U 1 1 5EDD3F73
P 5950 1650
F 0 "U8" H 5521 1696 50  0000 R CNN
F 1 "BME280" H 5521 1605 50  0000 R CNN
F 2 "_Sensores:Bosch_LGA-BME280" H 7450 1200 50  0001 C CNN
F 3 "https://ae-bst.resource.bosch.com/media/_tech/media/datasheets/BST-BME280-DS002.pdf" H 5950 1450 50  0001 C CNN
	1    5950 1650
	1    0    0    -1  
$EndComp
Wire Wire Line
	5850 1050 5850 900 
Wire Wire Line
	5850 900  5950 900 
Wire Wire Line
	6050 900  6050 1050
$Comp
L power:+3.3V #PWR077
U 1 1 5EDD3F7C
P 4750 900
F 0 "#PWR077" H 4750 750 50  0001 C CNN
F 1 "+3.3V" H 4900 1000 50  0000 C CNN
F 2 "" H 4750 900 50  0000 C CNN
F 3 "" H 4750 900 50  0000 C CNN
	1    4750 900 
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR080
U 1 1 5EDD3F8E
P 5950 2550
F 0 "#PWR080" H 5950 2300 50  0001 C CNN
F 1 "GND" H 5955 2377 50  0000 C CNN
F 2 "" H 5950 2550 50  0001 C CNN
F 3 "" H 5950 2550 50  0001 C CNN
	1    5950 2550
	1    0    0    -1  
$EndComp
$Comp
L Jumper:Jumper_3_Bridged12 JP4
U 1 1 5EDD3FA2
P 4300 1100
F 0 "JP4" H 4250 1250 50  0000 L CNN
F 1 "JP_3P_12Closed" H 3950 1350 50  0000 L CNN
F 2 "Jumper:SolderJumper-3_P1.3mm_Bridged12_RoundedPad1.0x1.5mm" H 4300 1100 50  0001 C CNN
F 3 "~" H 4300 1100 50  0001 C CNN
	1    4300 1100
	-1   0    0    1   
$EndComp
Wire Wire Line
	4050 1100 3850 1100
Wire Wire Line
	5950 2550 5950 2400
Wire Wire Line
	5950 2400 6050 2400
Wire Wire Line
	5950 750  5950 900 
Connection ~ 5950 900 
Wire Wire Line
	5950 900  6050 900 
$Comp
L power:GND #PWR063
U 1 1 5EDD3FBB
P 3850 1300
F 0 "#PWR063" H 3850 1050 50  0001 C CNN
F 1 "GND" H 3855 1127 50  0000 C CNN
F 2 "" H 3850 1300 50  0001 C CNN
F 3 "" H 3850 1300 50  0001 C CNN
	1    3850 1300
	1    0    0    -1  
$EndComp
Wire Wire Line
	950  950  1100 950 
$Comp
L Device:C C27
U 1 1 5EDD3FC8
P 2750 1200
F 0 "C27" H 2865 1246 50  0000 L CNN
F 1 "100nF" H 2865 1155 50  0000 L CNN
F 2 "Capacitor_SMD:C_0603_1608Metric_Pad1.05x0.95mm_HandSolder" H 2788 1050 50  0001 C CNN
F 3 "https://datasheet.lcsc.com/szlcsc/YAGEO-AC0603KRX7R9BB104_C149620.pdf" H 2750 1200 50  0001 C CNN
	1    2750 1200
	1    0    0    -1  
$EndComp
$Comp
L Device:C C26
U 1 1 5EDD3FCE
P 2450 1200
F 0 "C26" H 2000 1300 50  0000 L CNN
F 1 "100nF" H 2050 1200 50  0000 L CNN
F 2 "Capacitor_SMD:C_0603_1608Metric_Pad1.05x0.95mm_HandSolder" H 2488 1050 50  0001 C CNN
F 3 "https://datasheet.lcsc.com/szlcsc/YAGEO-AC0603KRX7R9BB104_C149620.pdf" H 2450 1200 50  0001 C CNN
	1    2450 1200
	1    0    0    -1  
$EndComp
Wire Wire Line
	2450 1050 2450 950 
Wire Wire Line
	2750 950  2750 1050
Wire Wire Line
	4750 1100 4550 1100
Wire Wire Line
	6550 1550 6900 1550
Wire Wire Line
	6550 1750 6900 1750
Wire Wire Line
	5850 2400 5850 2250
Wire Wire Line
	6050 2250 6050 2400
$Comp
L power:+3.3V #PWR079
U 1 1 5E53F32A
P 5950 750
F 0 "#PWR079" H 5950 600 50  0001 C CNN
F 1 "+3.3V" H 6100 850 50  0000 C CNN
F 2 "" H 5950 750 50  0000 C CNN
F 3 "" H 5950 750 50  0000 C CNN
	1    5950 750 
	1    0    0    -1  
$EndComp
Wire Wire Line
	6550 1350 6900 1350
Wire Wire Line
	6550 1950 6900 1950
$Comp
L power:GND #PWR062
U 1 1 5E545956
P 2600 1600
F 0 "#PWR062" H 2600 1350 50  0001 C CNN
F 1 "GND" H 2605 1427 50  0000 C CNN
F 2 "" H 2600 1600 50  0001 C CNN
F 3 "" H 2600 1600 50  0001 C CNN
	1    2600 1600
	1    0    0    -1  
$EndComp
Wire Wire Line
	2450 1350 2450 1500
Wire Wire Line
	2450 1500 2600 1500
Wire Wire Line
	2750 1500 2750 1350
Wire Wire Line
	2600 1500 2600 1600
Connection ~ 2600 1500
Wire Wire Line
	2600 1500 2750 1500
$Comp
L power:+3.3V #PWR059
U 1 1 5E546DA5
P 1550 850
F 0 "#PWR059" H 1550 700 50  0001 C CNN
F 1 "+3.3V" H 1700 950 50  0000 C CNN
F 2 "" H 1550 850 50  0000 C CNN
F 3 "" H 1550 850 50  0000 C CNN
	1    1550 850 
	1    0    0    -1  
$EndComp
Wire Wire Line
	4750 900  4750 1100
Wire Wire Line
	3850 1100 3850 1300
Wire Wire Line
	4300 950  4300 850 
Wire Wire Line
	4300 850  3850 850 
Text Label 3850 850  0    50   ~ 0
sdo
Text Label 6900 1350 0    50   ~ 0
sdo
Text Label 6900 1550 0    50   ~ 0
sck
Text Label 6900 1750 0    50   ~ 0
sdi
Text Label 6900 1950 0    50   ~ 0
csb
Connection ~ 5950 2400
Wire Wire Line
	5950 2400 5850 2400
Text Label 1550 1250 0    50   ~ 0
csb
Wire Wire Line
	2450 950  2600 950 
Wire Wire Line
	1550 850  1550 1250
Wire Wire Line
	950  850  1100 850 
Text Label 1100 850  0    50   ~ 0
sdi
Text Label 1100 950  0    50   ~ 0
sck
Wire Notes Line
	500  2350 5100 2350
Wire Notes Line
	5100 3000 7250 3000
Wire Notes Line
	500  2350 500  2650
Wire Notes Line
	500  2650 5100 2650
Wire Notes Line
	5100 3300 7250 3300
Wire Notes Line
	7250 500  7250 3300
Wire Notes Line
	5100 500  5100 3300
Wire Notes Line
	3400 500  3400 2650
Wire Notes Line
	1950 500  1950 2650
Wire Notes Line
	1400 500  1400 2650
Text Notes 600  2850 0    50   ~ 0
Salida etiquetas globales\n
Text Notes 1500 2550 0    50   ~ 0
Activación\nBME280
Text Notes 2250 2500 0    50   ~ 0
Alimentación BME 280
Text Notes 3650 2500 0    50   ~ 0
Selección dirección I2C BME 280
Text Notes 5700 3150 0    50   ~ 0
Sensor BME 280
$Comp
L power:+3.3V #PWR060
U 1 1 5E6B2A34
P 2600 850
F 0 "#PWR060" H 2600 700 50  0001 C CNN
F 1 "+3.3V" H 2750 950 50  0000 C CNN
F 2 "" H 2600 850 50  0000 C CNN
F 3 "" H 2600 850 50  0000 C CNN
	1    2600 850 
	1    0    0    -1  
$EndComp
Wire Wire Line
	2600 850  2600 950 
Connection ~ 2600 950 
Wire Wire Line
	2600 950  2750 950 
Text GLabel 950  950  0    50   Input ~ 0
PB8|SCL
Text GLabel 950  850  0    50   Input ~ 0
PB9|SDA
$EndSCHEMATC

EESchema Schematic File Version 4
EELAYER 30 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 6 9
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L Device:R R14
U 1 1 60DCEE84
P 1000 1100
F 0 "R14" H 931 1054 50  0000 R CNN
F 1 "1k" H 931 1145 50  0000 R CNN
F 2 "Resistor_SMD:R_0805_2012Metric_Pad1.15x1.40mm_HandSolder" V 930 1100 50  0001 C CNN
F 3 "~" H 1000 1100 50  0001 C CNN
	1    1000 1100
	1    0    0    1   
$EndComp
Wire Wire Line
	1000 1250 1000 1300
Wire Wire Line
	1000 1300 1200 1300
Wire Wire Line
	1000 950  1000 800 
Wire Wire Line
	1200 1500 1000 1500
Wire Wire Line
	1800 1300 1900 1300
Wire Wire Line
	1900 1300 1900 1200
Text GLabel 1900 1200 1    50   Input ~ 0
VCC_COM
$Comp
L Device:D D7
U 1 1 60DD10BF
P 2100 1700
F 0 "D7" V 2054 1779 50  0000 L CNN
F 1 "mra4007" V 2145 1779 50  0000 L CNN
F 2 "Diode_SMD:D_SMA" H 2100 1700 50  0001 C CNN
F 3 "~" H 2100 1700 50  0001 C CNN
	1    2100 1700
	0    1    1    0   
$EndComp
Wire Wire Line
	1800 1500 2100 1500
$Comp
L Relay:G5V-1 K1
U 1 1 60DD1CA9
P 4150 1650
F 0 "K1" V 3583 1650 50  0000 C CNN
F 1 "G5V-1" V 3674 1650 50  0000 C CNN
F 2 "Relay_THT:Relay_SPDT_Omron_G5V-1" H 5280 1620 50  0001 C CNN
F 3 "http://omronfs.omron.com/en_US/ecb/products/pdf/en-g5v_1.pdf" H 4150 1650 50  0001 C CNN
	1    4150 1650
	0    1    1    0   
$EndComp
Text Label 1850 1500 0    50   ~ 0
Coil_K1
Text Label 4450 1450 0    50   ~ 0
Coil_K1
$Comp
L power:GND #PWR057
U 1 1 60DD6CEC
P 3800 1350
F 0 "#PWR057" H 3800 1100 50  0001 C CNN
F 1 "GND" H 3805 1177 50  0000 C CNN
F 2 "" H 3800 1350 50  0001 C CNN
F 3 "" H 3800 1350 50  0001 C CNN
	1    3800 1350
	-1   0    0    1   
$EndComp
Wire Wire Line
	3800 1350 3800 1450
Wire Wire Line
	3800 1450 3850 1450
Text GLabel 3850 1850 0    50   Input ~ 0
VAC_L
$Comp
L Device:R R15
U 1 1 60DDA694
P 2400 1500
F 0 "R15" V 2193 1500 50  0000 C CNN
F 1 "1k" V 2284 1500 50  0000 C CNN
F 2 "Resistor_SMD:R_0805_2012Metric_Pad1.15x1.40mm_HandSolder" V 2330 1500 50  0001 C CNN
F 3 "~" H 2400 1500 50  0001 C CNN
	1    2400 1500
	0    -1   1    0   
$EndComp
$Comp
L Device:LED D8
U 1 1 60DDAEA8
P 2650 1750
F 0 "D8" V 2689 1633 50  0000 R CNN
F 1 "LED" V 2598 1633 50  0000 R CNN
F 2 "LED_SMD:LED_0805_2012Metric_Pad1.15x1.40mm_HandSolder" H 2650 1750 50  0001 C CNN
F 3 "~" H 2650 1750 50  0001 C CNN
	1    2650 1750
	0    -1   -1   0   
$EndComp
Wire Wire Line
	2100 1500 2100 1550
Wire Wire Line
	1000 1500 1000 1950
Text Label 4450 1950 0    50   ~ 0
NO-K1
$Comp
L Connector_Generic:Conn_01x03 J12
U 1 1 60DE4537
P 5900 1700
F 0 "J12" H 5980 1742 50  0000 L CNN
F 1 "Conn_01x03" H 5980 1651 50  0000 L CNN
F 2 "Connector_Phoenix_MC_HighVoltage:PhoenixContact_MCV_1,5_3-G-5.08_1x03_P5.08mm_Vertical" H 5900 1700 50  0001 C CNN
F 3 "~" H 5900 1700 50  0001 C CNN
	1    5900 1700
	1    0    0    -1  
$EndComp
Text Label 5400 1600 0    50   ~ 0
NO-K1
Wire Wire Line
	5400 1600 5700 1600
Text Label 4450 1750 0    50   ~ 0
NC-K1
Text Label 5400 1700 0    50   ~ 0
NC-K1
Wire Wire Line
	5400 1700 5700 1700
Text GLabel 5650 1800 0    50   Input ~ 0
VAC_N
Wire Wire Line
	5650 1800 5700 1800
$Comp
L Device:R R16
U 1 1 60DEED6F
P 1000 2800
F 0 "R16" H 931 2754 50  0000 R CNN
F 1 "1k" H 931 2845 50  0000 R CNN
F 2 "Resistor_SMD:R_0805_2012Metric_Pad1.15x1.40mm_HandSolder" V 930 2800 50  0001 C CNN
F 3 "~" H 1000 2800 50  0001 C CNN
	1    1000 2800
	1    0    0    1   
$EndComp
Wire Wire Line
	1000 2950 1000 3000
Wire Wire Line
	1000 3000 1200 3000
Wire Wire Line
	1000 2650 1000 2500
Wire Wire Line
	1200 3200 1000 3200
Wire Wire Line
	1800 3000 1900 3000
Wire Wire Line
	1900 3000 1900 2800
Text GLabel 1900 2800 1    50   Input ~ 0
VCC_COM
$Comp
L Device:D D9
U 1 1 60DEED7E
P 2100 3400
F 0 "D9" V 2054 3479 50  0000 L CNN
F 1 "mra4007" V 2145 3479 50  0000 L CNN
F 2 "Diode_SMD:D_SMA" H 2100 3400 50  0001 C CNN
F 3 "~" H 2100 3400 50  0001 C CNN
	1    2100 3400
	0    1    1    0   
$EndComp
Wire Wire Line
	1800 3200 2100 3200
Text Label 1850 3200 0    50   ~ 0
Coil_K2
$Comp
L Device:R R20
U 1 1 60DEED9C
P 2400 3200
F 0 "R20" V 2193 3200 50  0000 C CNN
F 1 "1k" V 2284 3200 50  0000 C CNN
F 2 "Resistor_SMD:R_0805_2012Metric_Pad1.15x1.40mm_HandSolder" V 2330 3200 50  0001 C CNN
F 3 "~" H 2400 3200 50  0001 C CNN
	1    2400 3200
	0    -1   1    0   
$EndComp
$Comp
L Device:LED D13
U 1 1 60DEEDA2
P 2650 3450
F 0 "D13" V 2689 3333 50  0000 R CNN
F 1 "LED" V 2598 3333 50  0000 R CNN
F 2 "LED_SMD:LED_0805_2012Metric_Pad1.15x1.40mm_HandSolder" H 2650 3450 50  0001 C CNN
F 3 "~" H 2650 3450 50  0001 C CNN
	1    2650 3450
	0    -1   -1   0   
$EndComp
Wire Wire Line
	2100 3200 2250 3200
Connection ~ 2100 3200
Wire Wire Line
	2100 3200 2100 3250
Wire Wire Line
	1000 3200 1000 3650
$Comp
L Connector_Generic:Conn_01x03 J13
U 1 1 60DEEDB4
P 5950 3350
F 0 "J13" H 6030 3392 50  0000 L CNN
F 1 "Conn_01x03" H 6030 3301 50  0000 L CNN
F 2 "Connector_Phoenix_MC_HighVoltage:PhoenixContact_MCV_1,5_3-G-5.08_1x03_P5.08mm_Vertical" H 5950 3350 50  0001 C CNN
F 3 "~" H 5950 3350 50  0001 C CNN
	1    5950 3350
	1    0    0    -1  
$EndComp
Wire Wire Line
	5450 3250 5750 3250
Wire Wire Line
	5450 3350 5750 3350
Text GLabel 5700 3450 0    50   Input ~ 0
VAC_N
Wire Wire Line
	5700 3450 5750 3450
$Comp
L Relay:G5V-1 K2
U 1 1 60DF86B5
P 4100 3300
F 0 "K2" V 3533 3300 50  0000 C CNN
F 1 "G5V-1" V 3624 3300 50  0000 C CNN
F 2 "Relay_THT:Relay_SPDT_Omron_G5V-1" H 5230 3270 50  0001 C CNN
F 3 "http://omronfs.omron.com/en_US/ecb/products/pdf/en-g5v_1.pdf" H 4100 3300 50  0001 C CNN
	1    4100 3300
	0    1    1    0   
$EndComp
Text Label 4400 3100 0    50   ~ 0
Coil_K2
$Comp
L power:GND #PWR061
U 1 1 60DF86C3
P 3750 3000
F 0 "#PWR061" H 3750 2750 50  0001 C CNN
F 1 "GND" H 3755 2827 50  0000 C CNN
F 2 "" H 3750 3000 50  0001 C CNN
F 3 "" H 3750 3000 50  0001 C CNN
	1    3750 3000
	-1   0    0    1   
$EndComp
Wire Wire Line
	3750 3000 3750 3100
Wire Wire Line
	3750 3100 3800 3100
Text GLabel 3800 3500 0    50   Input ~ 0
VAC_L
Text Label 4400 3600 0    50   ~ 0
NO-K2
Text Label 4400 3400 0    50   ~ 0
NC-K2
$Comp
L power:GND #PWR048
U 1 1 60E65A05
P 1000 2000
F 0 "#PWR048" H 1000 1750 50  0001 C CNN
F 1 "GND" H 1005 1827 50  0000 C CNN
F 2 "" H 1000 2000 50  0001 C CNN
F 3 "" H 1000 2000 50  0001 C CNN
	1    1000 2000
	1    0    0    -1  
$EndComp
Wire Wire Line
	1000 1950 2100 1950
Connection ~ 1000 1950
Wire Wire Line
	1000 1950 1000 2000
Wire Wire Line
	2100 1950 2650 1950
Wire Wire Line
	2650 1950 2650 1900
Connection ~ 2100 1950
$Comp
L power:GND #PWR058
U 1 1 60E691D0
P 1000 3700
F 0 "#PWR058" H 1000 3450 50  0001 C CNN
F 1 "GND" H 1005 3527 50  0000 C CNN
F 2 "" H 1000 3700 50  0001 C CNN
F 3 "" H 1000 3700 50  0001 C CNN
	1    1000 3700
	1    0    0    -1  
$EndComp
Wire Wire Line
	1000 3650 2100 3650
Connection ~ 1000 3650
Wire Wire Line
	1000 3650 1000 3700
Wire Wire Line
	2100 3650 2650 3650
Wire Wire Line
	2650 3650 2650 3600
Connection ~ 2100 3650
Wire Wire Line
	2650 3200 2650 3300
Wire Wire Line
	2100 3550 2100 3650
Wire Wire Line
	2550 3200 2650 3200
Wire Wire Line
	2100 1500 2250 1500
Connection ~ 2100 1500
Wire Wire Line
	2550 1500 2650 1500
Wire Wire Line
	2650 1500 2650 1600
Wire Wire Line
	2100 1850 2100 1950
Text Label 5450 3350 0    50   ~ 0
NC-K2
Text Label 5450 3250 0    50   ~ 0
NO-K2
Text GLabel 1000 2500 0    50   Input ~ 0
PB13|D13
Text GLabel 1000 800  0    50   Input ~ 0
PB12|D10
$Comp
L Connector_Generic:Conn_01x02 J17
U 1 1 610231EB
P 9600 4700
F 0 "J17" H 9680 4692 50  0000 L CNN
F 1 "Conn_01x02" H 9680 4601 50  0000 L CNN
F 2 "Connector_Phoenix_MC_HighVoltage:PhoenixContact_MCV_1,5_2-G-5.08_1x02_P5.08mm_Vertical" H 9600 4700 50  0001 C CNN
F 3 "~" H 9600 4700 50  0001 C CNN
	1    9600 4700
	1    0    0    -1  
$EndComp
Text GLabel 9400 4700 0    50   Input ~ 0
VAC_L
Text GLabel 9400 4800 0    50   Input ~ 0
VAC_N
$Comp
L Isolator:TLP185 U6
U 1 1 60D062B0
P 1500 1400
F 0 "U6" H 1500 1725 50  0000 C CNN
F 1 "TLP185" H 1500 1634 50  0000 C CNN
F 2 "Package_SO:SOIC-4_4.55x3.7mm_P2.54mm" H 1500 1100 50  0001 C CIN
F 3 "https://toshiba.semicon-storage.com/info/docget.jsp?did=11791&prodName=TLP185" H 1500 1400 50  0001 L CNN
	1    1500 1400
	1    0    0    -1  
$EndComp
$Comp
L Isolator:TLP185 U7
U 1 1 60D119DC
P 1500 3100
F 0 "U7" H 1500 3425 50  0000 C CNN
F 1 "TLP185" H 1500 3334 50  0000 C CNN
F 2 "Package_SO:SOIC-4_4.55x3.7mm_P2.54mm" H 1500 2800 50  0001 C CIN
F 3 "https://toshiba.semicon-storage.com/info/docget.jsp?did=11791&prodName=TLP185" H 1500 3100 50  0001 L CNN
	1    1500 3100
	1    0    0    -1  
$EndComp
$EndSCHEMATC

EESchema Schematic File Version 4
EELAYER 30 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 1 9
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Sheet
S 850  1000 1500 1050
U 60C45676
F0 "murataLoraWan" 50
F1 "murataLoRaWAN.sch" 50
$EndSheet
$Sheet
S 6690 980  1350 1050
U 60FC0FF2
F0 "boostdcdc" 50
F1 "boostdcdc.sch" 50
$EndSheet
$Sheet
S 6710 2280 1500 1050
U 60C45B0E
F0 "mcp73831" 50
F1 "mcp73831.sch" 50
$EndSheet
$Sheet
S 6750 3550 1500 1050
U 60C775C8
F0 "ac2dc" 50
F1 "ac2dc.sch" 50
$EndSheet
$Sheet
S 6750 4850 1350 1000
U 60DCC650
F0 "relayACOut" 50
F1 "relayACOut.sch" 50
$EndSheet
$Sheet
S 2510 4900 1490 1030
U 60C864F6
F0 "GPSL86" 50
F1 "GPSL86.sch" 50
$EndSheet
$Sheet
S 800  4900 1550 1050
U 60D337EA
F0 "BME280" 50
F1 "BME280.sch" 50
$EndSheet
$Sheet
S 4200 4900 1450 1000
U 60D3388F
F0 "adxl345" 50
F1 "adxl345.sch" 50
$EndSheet
$EndSCHEMATC
